FROM node:18-alpine as build

ARG SSH_PRIVATE_KEY

WORKDIR /app
RUN apk update && apk add git
RUN apk add --no-cache openssh-client
RUN mkdir -p -m 400 /root/.ssh
RUN ssh-keyscan bitbucket.org >> /root/.ssh/known_hosts

#production with environment variables
FROM build as prod
ENV PORT=8002
ENV NODE_ENV=prod
COPY . .
RUN echo "${SSH_PRIVATE_KEY}" > /root/.ssh/id_rsa
RUN chmod 400 -R /root/.ssh
COPY package.json yarn.lock ./
RUN yarn install
EXPOSE 8002
RUN yarn run build
CMD ["node", "dist/main"]