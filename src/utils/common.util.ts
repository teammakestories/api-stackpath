import { CURRENT_DOMAIN_CONFIG } from 'src/config/app.config';

const { INTERNAL_DOMAIN } = CURRENT_DOMAIN_CONFIG;

import * as http from 'https';
import * as moment from 'moment';
import { HttpStatus, HttpException } from '@nestjs/common';
import {
  GuardWithCodeInterface,
  HTTPResponse,
} from 'nest-library/dist/interface/util.interface';

export const resolver = async (promise: any) => {
  try {
    const data = await promise;
    return [data, null];
  } catch (error) {
    return [null, error];
  }
};

export const makeInternalDomain = (
  userDomain: string,
): { name: string; domain: string } => {
  const transformedDomain = userDomain.replace(/\./g, '-');
  return {
    name: transformedDomain,
    domain: `${transformedDomain}.${INTERNAL_DOMAIN}`,
  };
};

export const paginationStages = ({ limit = 10, page = 1 }) => {
  const skip = limit * (page - 1);
  return [{ $skip: skip }, { $limit: limit }];
};

export const searchStages = ({ field, searchTerm, match = {} }: any) => [
  {
    $match: {
      $text: {
        $search: searchTerm,
        $caseSensitive: false,
      },

      ...match,
    },
  },
];

export const sortStages = ({ sortBy, sortOrder }: any) => {
  const sort = sortBy ? { [sortBy]: sortOrder || 1 } : {};
  return [
    {
      $sort: {
        ...sort,
      },
    },
  ];
};

export const withPaginationMetadata = ({
  fieldName = 'data',
  data,
  count = 0,
  limit = 10,
  page = 1,
}: any) => {
  const pages = Math.ceil(count / limit ?? 10);
  return {
    [fieldName]: data,
    meta: {
      total: count,
      limit,
      page,
      pages,
    },
  };
};

export const cleanURL = (url = '') => {
  let newUrl = url;
  // remove http and https from url
  newUrl = newUrl.replace(/^https?\:\/\//i, '');

  // remove www. from url
  newUrl = newUrl.replace(/^www\./i, '');

  return newUrl;
};

export const getDomain = (url = '') => {
  const domain = new URL(url);
  return domain.hostname;
};
export const replaceSlash = (url = '') => url.replace(/\//g, '-');

export const fetch = (url: string, options = {}) => {
  return new Promise((resolve, reject) => {
    http
      .request(url, { method: 'get', timeout: 5000 }, (resp) => {
        let data = '';

        // A chunk of data has been received.
        resp.on('data', (chunk) => {
          data += chunk;
        });

        // The whole response has been received. Print out the result.
        resp.on('end', () => {
          resolve(JSON.parse(data).explanation);
        });
      })
      .on('error', (err) => {
        reject(err);
      });
  });
};

export const allowAction = (createdTime) => {
  const dateDiff = moment.duration(
    moment(new Date()).diff(moment(createdTime)),
  );
  return dateDiff.asHours() >= 24;
};

export const STATUS = Object.freeze({
  SUCCESS: 'success',
  ERROR: 'error',
});

export const guard = ({
  error,
  condition,
  message,
  code = HttpStatus.BAD_REQUEST,
  data,
}: GuardWithCodeInterface) => {
  if (error || condition) {
    return sendResponse({
      code,
      status: STATUS.ERROR,
      message: message || error?.message || 'Something Went Wrong!',
      data,
    });
  }
};

export const sendResponse = ({
  status = STATUS.SUCCESS,
  code = HttpStatus.OK,
  data = null,
  message = null,
}: HTTPResponse) => {
  if (status === STATUS.SUCCESS) {
    return {
      status,
      code,
      data,
      message,
    };
  } else {
    throw new HttpException(
      {
        status,
        code,
        data,
        message,
      },
      code,
    );
  }
};
