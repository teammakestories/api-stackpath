import * as process from 'process';
export const MONGODB_URI = process.env.MONGODB_URI2;
export const MONGO_DB = process.env.MONGO_DB || 'makestories';
export const MONGO_AUTH_SOURCE = process.env.MONGO_AUTH_SOURCE;
export const MONGO_REPLICA_SET = process.env.MONGO_REPLICA_SET;
export const MONGO_READ_PREFERENCE = process.env.MONGO_READ_PREFERENCE;
export const ENVIRONMENT = process.env.ENVIRONMENT;
export const makestoriesApiBaseUrl = 'https://api.makestories.io';
export const makestoriesNodeApiBaseUrl = process.env.MS_NODE_API;
export const MS_BILLING_BASE_URL = process.env.MS_BILLING_BASE_URL;
export const FIREBASE_ACCESS_TOKEN = process.env.FIREBASE_ACCESS_TOKEN;

export const DOMAIN_CONFIG = {
  config1: {
    CLOUDFLARE_ZONE: '99b3aa8d42b29593acde20526ecc98c1',
    INTERNAL_DOMAIN: 'webstory.link',
    TYPE: 'CLOUDFLARE',
  },
  config2: {
    CLOUDFLARE_ZONE: '0d6509931c9c663b128934b5c8f12cc9',
    INTERNAL_DOMAIN: 'webstories.link',
    TYPE: 'CLOUDFLARE',
  },
  config3: {
    CLOUDFLARE_ZONE: 'de483987209956395eff16040467214a',
    INTERNAL_DOMAIN: 'webstory.website',
    TYPE: 'CLOUDFLARE',
  },
  config4: {
    INTERNAL_DOMAIN: 'stories.center',
    TYPE: 'CLOUDNS',
    ARN: 'arn:aws:acm:us-east-1:568219205919:certificate/486cbd7d-d245-48b8-9f36-240570ce51c6',
  },
};

export const CURRENT_DOMAIN_CONFIG = DOMAIN_CONFIG[process.env.DOMAIN_CONFIG];

export const getMongoConfig = (): any =>
  ENVIRONMENT === 'PRODUCTION'
    ? {
        authSource: MONGO_AUTH_SOURCE,
        replicaSet: MONGO_REPLICA_SET,
        dbName: MONGO_DB,
        readPreference: MONGO_READ_PREFERENCE,
      }
    : { dbName: MONGO_DB };
