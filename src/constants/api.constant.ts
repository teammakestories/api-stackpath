import {
  MS_BILLING_BASE_URL,
  makestoriesApiBaseUrl,
  makestoriesNodeApiBaseUrl,
} from 'src/config/app.config';

export const HOSTING_SETUP_URL = `${makestoriesNodeApiBaseUrl}/hosting/setup/step-2`;

export const MS_BILLING = Object.freeze({
  SUBSCRIPTIONS: {
    HANDLE_MODIFIER: `${MS_BILLING_BASE_URL}/subscriptions/modifier`,
    PLAN_DETAILS: `${MS_BILLING_BASE_URL}/subscriptions/plan-details`,
    CHECK_MODIFIER: `${MS_BILLING_BASE_URL}/subscriptions/modifiers/check`,
    VIEW_MODIFIERS: `${MS_BILLING_BASE_URL}/subscriptions/modifiers/view`,
  },
  ACTIVITY_LOGS: {
    RECORD_ACTIVITY: `${MS_BILLING_BASE_URL}/activity/record`,
  },
});
