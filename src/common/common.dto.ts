import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsInt, IsOptional, IsString } from 'class-validator';

export class PaginationQueryDto {
  @IsOptional()
  @IsInt()
  @Type(() => Number)
  @ApiProperty({
    required: false,
    description: 'Number of document needs in one request | Default: 10',
    type: Number,
  })
  limit?: number;

  @IsOptional()
  @IsInt()
  @Type(() => Number)
  @ApiProperty({
    required: false,
    description: 'Page of the document | Default: 1',
    type: Number,
  })
  page?: number;

  @IsOptional()
  @IsString()
  @ApiProperty({
    required: false,
    description: 'Field name to be sorted',
    type: String,
  })
  sort?: string;

  @IsOptional()
  @IsString()
  @ApiProperty({
    required: false,
    description: 'Search',
    type: String,
  })
  search?: string;

  @IsOptional()
  @IsInt()
  @Type(() => Number)
  @ApiProperty({
    required: false,
    description:
      'To sort in Ascending order pass 1, to sort in descending order pass -1 | Default: 1',
    type: Number,
  })
  sortOrder?: number;
}
