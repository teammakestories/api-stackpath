import 'dotenv/config';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { HostingController } from './modules/hosting/hosting.controller';
import { resolver } from 'nest-library';

async function bootstrap() {
  const isCronJob = process.env.CRONJOB === 'CRONJOB';
  const time = process.env.TIME;
  const PORT = process.env.PORT || 8002;
  const app = await NestFactory.create(AppModule);
  app.enableCors();

  const config = new DocumentBuilder()
    .setTitle('API Hosting')
    .setDescription('This is the API for Hosting automation')
    .setVersion('1.0')
    .build();

  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('documentation', app, document);

  await app.listen(PORT, async () => {
    if (isCronJob) {
      const hostingServiceJob = app.get(HostingController);
      console.log('CRON STARTED FOR: ', time);
      switch (time) {
        case '10m':
          await resolver(hostingServiceJob.removeDomainDistribution());
          break;
        case '30m':
          await resolver(hostingServiceJob.requestVerificationCron());
          break;
        case '1d':
          await resolver(hostingServiceJob.removeDomainCRON());
          break;
        default:
          break;
      }
      console.log('CRON ENDED FOR: ', time);
      await app.close();
      process.exit();
    }
  });
}
bootstrap();
