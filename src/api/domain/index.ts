import axios from 'axios';
import { guard, guardV2, resolver } from 'nest-library';

export const getStoriesCountAPI = (channelID: string) =>
  axios({
    method: 'get',
    url: `https://api.makestories.io/channel/${channelID}/stories/count`,
    timeout: 30000,
  });

export const generateSiteMap = ({ userID, siteID }) =>
  axios({
    method: 'get',
    url: `https://server.makestories.io/sitemap-regenerate?user=${userID}&site=${siteID}`,
    timeout: 30000,
  });

export const getUserDetailAPI = async (userId: string) =>
  axios({
    method: 'get',
    url: `https://api.makestories.io/user/${userId}/get-details`,
    params: {
      token: 'VJuWN5sXzc8R9f9kaYSdWTEetz8dPRMGkW8bEMgjmW9xrMduBjY8sPt6AZ7C7DZ8',
    },
  });

export const sendMailAPI = async ({ email, domain }) =>
  axios({
    method: 'get',
    url: `https://server.makestories.io/send-mail?to=${email}.in&accessToken=GKGfsFh7MdHafJp9DEakKAEwJQ36CCGvbDz2FHEvKE6aJrCydzc7cgwwFC5w&subject=Domain Deletion Notice&body[type]=template&body[template]=domain-warning&body[data][domain]=${domain}`,
  });

export const sendWarningEmail = async ({ userId, domain }) => {
  const [data, error] = await resolver(getUserDetailAPI(userId));
  guardV2({ error });
  const [, error2] = await resolver(
    sendMailAPI({ email: data?.data?.email, domain }),
  );
  guardV2({ error: error2 });
};

export const createStoryListingAPI = ({ userId, siteId }) => {
  return axios({
    method: 'GET',
    url: 'https://lp.makestories.io/story-listing/create',
    params: {
      userId,
      siteId,
    },
    timeout: 1000,
  });
};
