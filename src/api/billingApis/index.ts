import axios from 'axios';
import { FIREBASE_ACCESS_TOKEN } from 'src/config/app.config';
import { MS_BILLING } from 'src/constants/api.constant';

const authParams = (userId: string) => {
  return {
    workspace: userId,
    user: userId,
    token: process.env.SERVER_TOKEN,
  };
};

export const handleModifierApi = ({ type, action, userId }) => {
  return axios({
    method: 'POST',
    url: MS_BILLING.SUBSCRIPTIONS.HANDLE_MODIFIER,
    params: authParams(userId),
    data: {
      type,
      action,
    },
  });
};

export const recordActivityApi = ({ data, action, ownerId }) => {
  return axios({
    method: 'POST',
    url: MS_BILLING.ACTIVITY_LOGS.RECORD_ACTIVITY,
    params: authParams(ownerId),
    data: {
      data,
      action,
    },
  });
};

export const checkModifierApi = (data: any) => {
  return axios({
    method: 'GET',
    url: MS_BILLING.SUBSCRIPTIONS.CHECK_MODIFIER,
    params: { ...authParams(data?.userId), type: data?.type },
  });
};

export const viewModifiersAPI = (userId: string) => {
  return axios({
    method: 'GET',
    url: MS_BILLING.SUBSCRIPTIONS.VIEW_MODIFIERS,
    params: authParams(userId),
  });
};

export const planDetailsApi = (data: any) => {
  return axios({
    method: 'GET',
    url: MS_BILLING.SUBSCRIPTIONS.PLAN_DETAILS,
    params: authParams(data?.userId),
  });
};
