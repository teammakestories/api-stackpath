import { DOMAIN_CONFIG } from 'src/config/app.config';

export const identifyZoneID = (url = '') => {
  if (!url) {
    return DOMAIN_CONFIG.config1.CLOUDFLARE_ZONE;
  }

  if (url.endsWith(DOMAIN_CONFIG.config1.INTERNAL_DOMAIN)) {
    return DOMAIN_CONFIG.config1.CLOUDFLARE_ZONE;
  }

  if (url.endsWith(DOMAIN_CONFIG.config2.INTERNAL_DOMAIN)) {
    return DOMAIN_CONFIG.config2.CLOUDFLARE_ZONE;
  }

  if (url.endsWith(DOMAIN_CONFIG.config3.INTERNAL_DOMAIN)) {
    return DOMAIN_CONFIG.config3.CLOUDFLARE_ZONE;
  }
};

export const identifyDomainType = (url = '') => {
  if (!url) {
    return DOMAIN_CONFIG.config1.CLOUDFLARE_ZONE;
  }
  for (const config in DOMAIN_CONFIG) {
    if (url.endsWith(DOMAIN_CONFIG[config].INTERNAL_DOMAIN)) {
      return DOMAIN_CONFIG[config].TYPE;
    }
  }
};

export const removeInternalDomain = (url = '') => {
  let newUrl = url;
  for (const key in DOMAIN_CONFIG) {
    const { INTERNAL_DOMAIN } = DOMAIN_CONFIG[key];
    newUrl = newUrl.replace(`.${INTERNAL_DOMAIN}`, '');
  }
  return newUrl;
};
