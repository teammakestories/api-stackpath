import { CURRENT_DOMAIN_CONFIG } from 'src/config/app.config';

export const getCloudflareURL = (
  zone = CURRENT_DOMAIN_CONFIG.CLOUDFLARE_ZONE,
) => `https://api.cloudflare.com/client/v4/zones/${zone}/dns_records`;
