import {
  DirectionType,
  DNSType,
  MatchType,
  OrderType,
} from './cloudflare.type';

export interface AddDnsInterface {
  type: DNSType;
  name: string;
  content: string;
  ttl?: number;
  priority?: number;
  proxied?: boolean;
  zone?: string;
}

export interface ListDnsInterface {
  match?: MatchType;
  name?: string;
  order?: OrderType;
  page?: number;
  per_page?: number;
  content?: string;
  type?: DNSType;
  proxied?: boolean;
  direction?: DirectionType;
}
