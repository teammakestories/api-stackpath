import axios from 'axios';
import { DOMAIN_CONFIG } from 'src/config/app.config';
import { getCloudflareURL } from './cloudflare.constant';
import { AddDnsInterface, ListDnsInterface } from './cloudflare.interface';
import { ConfigType } from './cloudflare.type';

export const getDnsRecords = async (
  query: ListDnsInterface,
  zoneID: string,
) => {
  return axios.get(getCloudflareURL(zoneID), {
    params: query,
    headers: {
      Authorization: `Bearer ${process.env.CLOUDFLARE_TOKEN}`,
      'Content-Type': 'application/json',
    },
  });
};

export const deleteDnsRecord = async (name = '', zoneID: string) => {
  const { data } = await getDnsRecords({ name, type: 'CNAME' }, zoneID);
  console.log({ data, result: data?.result });
  const id = data?.result[0]?.id ?? '';

  if (id) {
    return axios.delete(`${getCloudflareURL(zoneID)}/${id}`, {
      headers: {
        Authorization: `Bearer ${process.env.CLOUDFLARE_TOKEN}`,
        'Content-Type': 'application/json',
      },
    });
  }
};

export const addDnsRecord = (dnsData: AddDnsInterface) => {
  const { zone } = dnsData ?? {};
  return axios.post(getCloudflareURL(zone), dnsData, {
    headers: {
      Authorization: `Bearer ${process.env.CLOUDFLARE_TOKEN}`,
      'Content-Type': 'application/json',
    },
  });
};
