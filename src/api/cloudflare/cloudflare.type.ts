export type DNSType =
  | 'A'
  | 'AAAA'
  | 'CNAME'
  | 'HTTPS'
  | 'TXT'
  | 'SRV'
  | 'LOC'
  | 'MX'
  | 'NS'
  | 'CERT'
  | 'DNSKEY'
  | 'DS'
  | 'NAPTR'
  | 'SMIMEA'
  | 'SSHFP'
  | 'SVCB'
  | 'TLSA';

export type MatchType = 'all';

export type OrderType = 'type' | 'name' | 'content' | 'ttl' | 'proxied';

export type DirectionType = 'asc' | 'desc';

export type ConfigType = 'config1' | 'config2';
