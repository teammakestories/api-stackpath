import { Injectable } from '@nestjs/common';
import axios from 'axios';
import { guardV2, resolver } from 'nest-library';
import {
  ACCESS_HEADERS,
  ADD_CUSTOM_HOSTNAME,
  EGDE_CONFIG_URL,
  FORCE_SSL_URL,
  FREE_CERTIFICATE_URL,
  PULLZONE_URL,
  STATISTICS_URL,
} from './bunnycdn.constant';
import { GetStatisticsInterface } from './bunnycdn.interface';

@Injectable()
export class BunnycdnService {
  addPullZone({ name, isOurDomain = false }) {
    return axios.post(
      PULLZONE_URL,
      {
        Name: name,
        OriginUrl: isOurDomain
          ? process.env.FTP_HOSTNAME_2
          : process.env.FTP_HOSTNAME,
        Type: 1,
      },
      { headers: ACCESS_HEADERS },
    );
  }

  deletePullZone(pullZoneId: string) {
    return axios.delete(`${PULLZONE_URL}/${pullZoneId}`, {
      headers: ACCESS_HEADERS,
    });
  }

  private _addEdgeRules({ pullZoneId, headerValue }) {
    return axios.post(
      EGDE_CONFIG_URL(pullZoneId),
      {
        Triggers: [
          {
            PatternMatches: ['*'],
            Type: 0,
          },
        ],
        ActionType: 6,
        ActionParameter1: 'Host',
        ActionParameter2: headerValue,
        TriggerMatchingType: 0,
      },
      { headers: ACCESS_HEADERS },
    );
  }

  private _setForceSSL({ pullZoneId, hostName }) {
    return axios.post(
      FORCE_SSL_URL(pullZoneId),
      {
        ForceSSL: true,
        Hostname: hostName,
      },
      { headers: ACCESS_HEADERS },
    );
  }

  private _addCustomHostname({ pullZoneId, hostname }) {
    return axios.post(
      ADD_CUSTOM_HOSTNAME(pullZoneId),
      {
        Hostname: hostname,
      },
      { headers: ACCESS_HEADERS },
    );
  }

  loadFreeCertificate(hostname) {
    return axios.get(FREE_CERTIFICATE_URL, {
      headers: ACCESS_HEADERS,
      params: { hostname },
    });
  }

  getStatistics(params: GetStatisticsInterface) {
    return axios.get(STATISTICS_URL, { headers: ACCESS_HEADERS, params });
  }

  async addBunnyDomain({ site, pullZoneId, setEdgeRule = false }) {
    // Step 1: Add Custom Hostname
    const [hostName, hostNameError] = await resolver(
      this._addCustomHostname({
        pullZoneId,
        hostname: site,
      }),
    );

    guardV2({ error: hostNameError, message: 'Error adding custom hostname' });

    // Step 2: Add Edge Rules
    if (setEdgeRule) {
      const [edgeRule, edgeRuleError] = await resolver(
        this._addEdgeRules({
          pullZoneId,
          headerValue: site,
        }),
      );
      guardV2({ error: edgeRuleError, message: 'Error EdgeRule' });
    }

    // Step 3: Request SSL
    // const [freeCertificate, freeCertificateError] = await resolver(
    //   this.loadFreeCertificate(site),
    // );

    // Step 4: Set Force SSL
    const [forceSSL, forceSSLError] = await resolver(
      this._setForceSSL({
        pullZoneId,
        hostName: site,
      }),
    );
  }
}
