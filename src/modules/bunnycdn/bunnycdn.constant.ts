const BUNNY_BASE_URL = 'https://api.bunny.net';

export const PULLZONE_URL = `${BUNNY_BASE_URL}/pullzone`;

export const EGDE_CONFIG_URL = (pullZoneId: string) =>
  `${PULLZONE_URL}/${pullZoneId}/edgerules/addOrUpdate`;

export const FORCE_SSL_URL = (pullZoneId: string) =>
  `${PULLZONE_URL}/${pullZoneId}/setForceSSL`;

export const ADD_CUSTOM_HOSTNAME = (pullZoneId: string) =>
  `${PULLZONE_URL}/${pullZoneId}/addHostname`;

export const STATISTICS_URL = `${BUNNY_BASE_URL}/statistics`;

export const FREE_CERTIFICATE_URL = `${PULLZONE_URL}/loadFreeCertificate`;

export const ACCESS_HEADERS = {
  Accept: 'application/json',
  'Content-Type': 'application/json',
  AccessKey:
    '56be726d-39ab-45fe-8b40-5b0240937862dfb18567-4359-420c-bf24-47e896ff7832',
};

export const HOST_TYPE = Object.freeze({
  STACKPATH: 'STACKPATH',
  BUNNY: 'BUNNY',
});

export const SELECTED_HOST_TYPE = HOST_TYPE.STACKPATH;
