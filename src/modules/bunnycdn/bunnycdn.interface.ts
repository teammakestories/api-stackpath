export interface GetStatisticsInterface {
  dateFrom?: string;
  dateTo?: string;
  pullZone?: string;
  serverZoneId?: string;
  loadErrors?: boolean;
  hourly?: boolean;
}
