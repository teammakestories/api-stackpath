import { BunnycdnController } from './bunnycdn.controller';
import { BunnycdnService } from './bunnycdn.service';

import { Module } from '@nestjs/common';

@Module({
  imports: [],
  controllers: [BunnycdnController],
  providers: [BunnycdnService],
  exports: [BunnycdnService],
})
export class BunnycdnModule {}
