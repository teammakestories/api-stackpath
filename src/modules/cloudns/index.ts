import axios from 'axios';
import { getCloudNsURL } from './cloudns.constant';
import { CURRENT_DOMAIN_CONFIG } from 'src/config/app.config';

const { INTERNAL_DOMAIN } = CURRENT_DOMAIN_CONFIG;
// export const getDnsRecords = async () => {
//     return axios.get(getCloudflareURL(), {
//         params: query,
//         headers: {
//             Authorization: `Bearer ${process.env.CLOUDFLARE_TOKEN}`,
//             'Content-Type': 'application/json',
//         },
//     });
// };
export const deleteClouDnsRecord = async (_id: string) => {
  if (_id) {
    return axios.post(
      `${getCloudNsURL()}/delete-record.json?auth-id=${
        process.env.CLOUDNS_AUTH_ID
      }&auth-password=${process.env.CLOUDNS_AUTH_PASS}`,
      {
        'domain-name': INTERNAL_DOMAIN,
        'record-id': _id,
      },
    );
  }
};
export const addCloudNsRecord = (dnsData) => {
  return axios.post(
    `${getCloudNsURL()}/add-record.json?auth-id=${
      process.env.CLOUDNS_AUTH_ID
    }&auth-password=${process.env.CLOUDNS_AUTH_PASS}`,
    {
      ...dnsData,
    },
  );
};
