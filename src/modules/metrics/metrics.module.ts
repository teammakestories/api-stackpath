import { MetricsService } from './metrics.service';
import { MetricsController } from './metrics.controller';
import { Module } from '@nestjs/common';
import { StackpathModule } from '../stackpath/stackpath.module';

@Module({
  imports: [StackpathModule],
  controllers: [MetricsController],
  providers: [MetricsService],
  exports: [MetricsService],
})
export class MetricsModule {}
