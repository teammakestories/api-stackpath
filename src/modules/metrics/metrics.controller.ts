import {
  // BadRequestException,
  Controller,
  // Get,
  // HttpException,
  // HttpStatus,
  // Query,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
// import { ApiTags } from '@nestjs/swagger';
// import { guard, sendResponse } from 'nest-library';
// import { resolver } from 'src/utils/common.util';
import { StackpathService } from '../stackpath/stackpath.service';
// import {
//   GRANUALARITY,
//   GROUP_BY,
//   TOTAL_BANDWITH_USED,
//   TOTAL_REQUESTS,
// } from './metrics.constant';
// import { MetricsQueryDto } from './metrics.dto';
import { MetricsService } from './metrics.service';

// @ApiTags('Metric')
@Controller('v1/metric')
@UsePipes(new ValidationPipe({ transform: true }))
export class MetricsController {
  constructor(
    private stackpathService: StackpathService,
    private metricsService: MetricsService,
  ) {}

  // @Get('all')
  // async getMetrics(@Query() metricsQuery: MetricsQueryDto) {
  //   const [, tokenError] = await resolver(
  //     this.stackpathService.checkTokenValidity(),
  //   );
  //
  //   guard({
  //     error: tokenError,
  //     message: 'Forbidden',
  //     code: HttpStatus.FORBIDDEN,
  //   });
  //
  //   const options = {
  //     platforms: 'CDE',
  //     granularity: GRANUALARITY.P1D,
  //     sites: metricsQuery.sites,
  //     start_date: metricsQuery.startDate,
  //     end_date: metricsQuery.endDate,
  //   };
  //
  //   const [promiseAll, promiseAllError] = await resolver(
  //     Promise.all([
  //       this.stackpathService.getPops(),
  //       this.stackpathService.getMetrics(options),
  //       this.stackpathService.getMetrics({
  //         ...options,
  //         group_by: GROUP_BY.POP,
  //       }),
  //     ]),
  //   );
  //
  //   const [popsData, daily, dataCentres] = promiseAll || [];
  //   const pops = popsData?.data;
  //
  //   const dataCentresResults = dataCentres?.data?.data?.matrix?.results;
  //   const dailyResults = daily?.data?.data?.matrix?.results;
  //   const total = {
  //     totalBandwidth: this.metricsService.getTotalData(
  //       dailyResults,
  //       TOTAL_BANDWITH_USED,
  //     ),
  //     totalRequests: this.metricsService.getTotalData(
  //       dailyResults,
  //       TOTAL_REQUESTS,
  //     ),
  //   };
  //
  //   guard({ error: promiseAllError, message: 'Unable to get metrics' });
  //
  //   return sendResponse({
  //     data: {
  //       total,
  //       data: {
  //         bandwidthUsed: dailyResults.find(
  //           (k) => k?.metric?.__name__ === TOTAL_BANDWITH_USED,
  //         ),
  //         requests: dailyResults.find(
  //           (k) => k?.metric?.__name__ === TOTAL_REQUESTS,
  //         ),
  //       },
  //       dataCentres: this.metricsService.transformDataCentre(
  //         pops,
  //         dataCentresResults,
  //       ),
  //     },
  //   });
  // }
  // @Get('site/all')
  // async getSites() {
  //   try {
  //     await this.stackpathService.checkTokenValidity();
  //
  //     const [{ data }, error] = await resolver(
  //       this.stackpathService.getSites(),
  //     );
  //     if (error) throw new BadRequestException(error.message);
  //     return data;
  //   } catch (error) {
  //     throw new HttpException(error.message, 400);
  //   }
  // }
  // @Get('pops')
  // async getPops() {
  //   try {
  //     const [, tokenError] = await resolver(
  //       this.stackpathService.checkTokenValidity(),
  //     );
  //     if (tokenError) throw new BadRequestException(tokenError.message);
  //     const [{ data }, error] = await resolver(this.stackpathService.getPops());
  //     if (error) throw new BadRequestException(error.message);
  //     return data;
  //   } catch (error) {
  //     throw new HttpException(error.message, 400);
  //   }
  // }
}
