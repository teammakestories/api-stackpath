import { Injectable } from '@nestjs/common';
import { TOTAL_BANDWITH_USED, TOTAL_REQUESTS } from './metrics.constant';

@Injectable()
export class MetricsService {
  transformDataCentre(pops: any, dataCentresResults: any) {
    return pops.results.map((centre: any) => {
      const { code } = centre;
      const totalBandwith = dataCentresResults.find((c: any) => {
        return (
          c.metric.pop === code && c.metric.__name__ === TOTAL_BANDWITH_USED
        );
      });

      const reducer = (accumulator: any, current: any) =>
        accumulator + Number(current.value);

      const totalRequest = dataCentresResults.find((c: any) => {
        return c.metric.pop === code && c.metric.__name__ === TOTAL_REQUESTS;
      });
      return {
        ...centre,
        totalBandwidth: totalBandwith.values.reduce(reducer, 0),
        totalRequest: totalRequest.values.reduce(reducer, 0),
      };
    });
  }

  getTotalData(array: any, key: string) {
    const data = array.find((k) => k.metric.__name__ === key);

    const reducer = (accumulator, current) =>
      accumulator + Number(current.value);

    const total = data?.values?.reduce(reducer, 0) ?? 0;
    return total;
  }
}
