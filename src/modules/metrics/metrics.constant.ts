export const TOTAL_BANDWITH_USED = 'transfer_used_total_mb';
export const TOTAL_REQUESTS = 'delivery_http_requests_total';

export const GRANUALARITY = Object.freeze({
  AUTO: 'AUTO',
  PT5M: 'PT5M',
  PT1H: 'PT1H',
  P1D: 'P1D',
  P1M: 'P1M',
});

export const GROUP_BY = Object.freeze({
  NONE: 'NONE',
  SITE: 'SITE',
  PLATFORM: 'PLATFORM',
  POP: 'POP',
  REGION: 'REGION',
  STATUS: 'STATUS',
  STATUS_CATEGORY: 'STATUS_CATEGORY',
});
