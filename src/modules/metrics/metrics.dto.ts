import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';
import { AuthQueryDto } from 'nest-library';

type Granularity = 'AUTO' | 'PT5M' | 'PT1H' | 'P1D' | 'P1M';
type GroupBy =
  | 'NONE'
  | 'SITE'
  | 'PLATFORM'
  | 'POP'
  | 'REGION'
  | 'STATUS'
  | 'STATUS_CATEGORY';

export class MetricsQueryDto extends AuthQueryDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ description: 'Format: 2021-08-21T00:00:00Z' })
  startDate: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ description: 'Format: 2021-08-21T00:00:00Z' })
  endDate: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  sites: string;
  // granularity?: Granularity;

  // @IsString()
  // pops: string;

  // @IsString()
  // platforms?: string;

  // @IsString()
  // group_by?: GroupBy;
}
