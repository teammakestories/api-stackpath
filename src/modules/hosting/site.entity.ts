import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  DeleteDateColumn,
} from 'typeorm';

@Entity({ name: 'user_sites' })
export class UserSites {
  @PrimaryGeneratedColumn({ type: 'int' })
  id: number;

  @Column({ type: 'varchar', default: null, nullable: false, length: 255 })
  user_id: string;

  @Column({ type: 'varchar', default: null, nullable: false, length: 255 })
  site_url: string;

  @Column({ type: 'varchar', default: null, nullable: false, length: 255 })
  channel_id: string;

  @Column({ type: 'varchar', default: null, nullable: false, length: 255 })
  site_proxy_domain: string;

  @Column({ type: 'varchar', default: null, nullable: true, length: 255 })
  cache_clear_webhook: string;

  @Column({ type: 'varchar', default: null, nullable: true, length: 255 })
  callback_webhook: string;

  @Column({ type: 'varchar', default: null, nullable: true, length: 255 })
  sp_id: string;

  @Column({ type: 'varchar', default: null, nullable: true, length: 255 })
  sp_status: string;

  @Column({ type: 'varchar', default: null, nullable: true, length: 255 })
  domain_type: string;

  @Column({ type: 'varchar', default: null, nullable: true, length: 255 })
  certificate_id: string;

  @Column({ type: 'varchar', default: null, nullable: true, length: 255 })
  reverseproxy_endpoint: string;

  @Column({ type: 'tinyint', default: null, nullable: true })
  is_verified: boolean;

  @Column({ type: 'tinyint', default: null, nullable: true })
  is_ssl_verified: boolean;

  @Column({
    nullable: false,
    default: 0,
  })
  isDisabled: boolean;

  @Column({ type: 'tinyint', default: null, nullable: true })
  lp_status: boolean;

  @Column({ type: 'varchar', default: null, nullable: true, length: 255 })
  pull_zone_id: string;

  @Column({ type: 'varchar', default: null, nullable: true, length: 50 })
  ssl_dns_id: string;

  @Column({ type: 'varchar', default: null, nullable: true, length: 50 })
  dns_id: string;

  @Column({ type: 'varchar', default: null, nullable: true, length: 255 })
  cf_distribution_id: string;

  @Column({ type: 'varchar', default: null, nullable: true, length: 500 })
  ssl_cname_record: string;

  @Column({ type: 'varchar', default: null, nullable: true, length: 500 })
  ssl_cname_value: string;

  @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  created_at: Date;

  @Column({ type: 'timestamp' })
  warning_sent: Date;

  @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  updated_at: Date;

  @Column({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP',
    nullable: true,
  })
  @DeleteDateColumn()
  deleted_at: Date;
}
