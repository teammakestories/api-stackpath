import { BLOCKED_DOMAINS, OUR_DOMAIN } from './hosting.constant';

export const checkOurDomain = (domain = '') =>
  domain.includes(`${OUR_DOMAIN}/`);

export const removeHttps = (domain = '') => domain.replace('https://', '');

export const validate = ({ domain = '', keywords = [''], exact = false }) => {
  if (domain === '') {
    return false;
  }

  for (let i = 0; i < keywords.length; i++) {
    const keyword = keywords[i];

    if (exact) {
      if (domain === keyword) {
        return false;
      }
    } else {
      if (domain.includes(keyword)) {
        return false;
      }
    }
  }

  return true;
};
