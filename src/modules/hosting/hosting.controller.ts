import {
  Body,
  Controller,
  Delete,
  Get,
  // HttpStatus,
  Param,
  // Patch,
  Post,
  Query,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { unlinkSync, writeFileSync } from 'fs';

import { AuthQueryDto, resolver, withPaginationMetadata } from 'nest-library';
// import { addDnsRecord } from 'src/api/cloudflare';
import { PaginationQueryDto } from 'src/common/common.dto';
import {
  // cleanURL,
  // makeInternalDomain,
  // replaceSlash,
  allowAction,
  guard,
  sendResponse,
} from 'src/utils/common.util';
import { In, Like } from 'typeorm';
import { BunnycdnService } from '../bunnycdn/bunnycdn.service';
import { GRANUALARITY } from '../metrics/metrics.constant';
import { StackpathService } from '../stackpath/stackpath.service';
import {
  // ALLOWED_FIELDS_TO_UPDATE,
  // BLOCKED_DOMAINS,
  // BLOCKED_KEYWORDS,
  DOMAIN_TYPE,
  // OUR_DOMAIN,
} from './hosting.constant';
import {
  AddSiteDto,
  AddSiteV2Dto,
  // CdnControlDto,
  // EditSiteDto,
  GetAllSiteAdminDto,
  // GetEverySitesDto,
  GetSitesDto,
} from './hosting.dto';
import { HostingService } from './hosting.service';
import {
  checkOurDomain,
  removeHttps,
  // validate
} from './hosting.util';
// import {
//   identifyZoneID,
//   removeInternalDomain,
// } from 'src/api/cloudflare/cloudflare.util';
import { AwsService } from '../aws/aws.service';
import {
  planDetailsApi,
  handleModifierApi,
  recordActivityApi,
  viewModifiersAPI,
} from 'src/api/billingApis';

@ApiTags('Hosting')
@Controller('v1/hosting/site')
@UsePipes(new ValidationPipe({ transform: true }))
export class HostingController {
  constructor(
    private readonly stackpathService: StackpathService,
    private readonly hostingService: HostingService,
    private readonly bunnycdnService: BunnycdnService,
    private readonly awsService: AwsService,
  ) {}

  // @Patch('edit/admin/:id')
  // async editSite(
  //   @Param('id') id: string,
  //   @Body() body: EditSiteDto,
  //   @Query('token') token: string,
  // ) {
  //   guard({
  //     condition: token !== process.env.SERVER_TOKEN,
  //     code: 403,
  //     message: 'Forbidden',
  //   });
  //
  //   const [site, siteError] = await resolver(
  //     this.hostingService.getSiteDB({
  //       id: Number(id),
  //     }),
  //   );
  //
  //   guard({
  //     error: siteError,
  //     condition: !site,
  //     message: 'Site not found',
  //     code: 404,
  //   });
  //
  //   const dataToUpdate: any = {};
  //   for (const key in body) {
  //     if (ALLOWED_FIELDS_TO_UPDATE.has(key)) {
  //       dataToUpdate[key] = body[key];
  //     }
  //   }
  //
  //   const [updateSiteSuccess, updateSiteError] = await resolver(
  //     this.hostingService.updateSiteDB({ id }, dataToUpdate),
  //   );
  //
  //   guard({ error: updateSiteError, message: 'Unable to update site' });
  //
  //   return sendResponse({ message: 'Site Updated' });
  // }

  // @Post('add')
  // async addSite(@Body() siteData: AddSiteDto, @Query() query: AuthQueryDto) {
  //   const { workspace } = query;
  //
  //   const {
  //     domain = '',
  //     subDomain = '',
  //     type = DOMAIN_TYPE.SUBSOMAIN,
  //   } = siteData ?? {};
  //
  //   // Domain Validation Guards
  //   guard({ condition: !domain, message: 'Domain is required' });
  //
  //   guard({
  //     condition: !subDomain,
  //     message: 'Sub Domain is required',
  //   });
  //
  //   const isDomainValid = validate({
  //     domain,
  //     keywords: BLOCKED_DOMAINS,
  //     exact: true,
  //   });
  //
  //   const isSubdomainValid = validate({
  //     domain: subDomain,
  //     keywords: BLOCKED_KEYWORDS,
  //     exact: true,
  //   });
  //
  //   if (type === DOMAIN_TYPE.SUBSOMAIN) {
  //     guard({
  //       condition: !isSubdomainValid,
  //       message: 'Subdomain is not allowed',
  //     });
  //   }
  //
  //   guard({
  //     condition: !isDomainValid,
  //     message: 'Domain is not allowed',
  //   });
  //
  //   const cleanDomain = cleanURL(domain).toLowerCase();
  //   const cleanSubdomain = cleanURL(subDomain).toLowerCase();
  //
  //   guard({
  //     condition: cleanDomain === cleanSubdomain,
  //     message: 'Domain and Sub Domain should not be same',
  //   });
  //
  //   guard({
  //     condition: cleanSubdomain.includes('.com'),
  //     message: 'Subdomain should not contain ".com"',
  //   });
  //
  //   // Collection of domains and domain related data
  //
  //   // TODO: Convert all the domains to lowercase
  //   const domains = {
  //     subDomain: `${replaceSlash(cleanSubdomain)}.${cleanDomain}`,
  //     reverseProxyDomain: `${cleanDomain}/${cleanSubdomain}`,
  //     isReverseProxy: type === DOMAIN_TYPE.REVERSE_PROXY,
  //
  //     checkIsOurDomain() {
  //       return domain === OUR_DOMAIN && this.isReverseProxy;
  //     },
  //
  //     getMainDomain() {
  //       return this.isReverseProxy ? this.reverseProxyDomain : this.subDomain;
  //     },
  //     getInternalDomain() {
  //       // webstory.link url
  //       return makeInternalDomain(this.subDomain);
  //     },
  //   };
  //
  //   const isOurDomain = domains.checkIsOurDomain();
  //   const internal = domains.getInternalDomain();
  //
  //   console.log({
  //     domains,
  //     isOurDomain,
  //     internal,
  //     mainDomain: domains.getMainDomain(),
  //   });
  //
  //   // Checking if domain Exist
  //   const withHttps = `https://${internal.domain}`;
  //   const [existingDomain, existingDomainError] = await resolver(
  //     this.hostingService.getSiteDB({
  //       site_proxy_domain: withHttps,
  //     }),
  //   );
  //
  //   const condition =
  //     existingDomain?.site_proxy_domain === withHttps &&
  //     existingDomain?.deleted_at === null;
  //
  //   console.log({ existingDomain, existingDomainError, condition, withHttps });
  //   guard({
  //     condition,
  //     message: 'Domain already exist',
  //     code: 409,
  //   });
  //
  //   console.log('after guard 1');
  //   const bunnyName = `cdn-bunny-${internal.name}`;
  //
  //   const [pullZone, pullZoneError] = await resolver(
  //     this.bunnycdnService.addPullZone({ name: internal?.name, isOurDomain }),
  //   );
  //
  //   console.log({
  //     pullZone,
  //     pullZoneError,
  //   });
  //
  //   const { Id, Name, CnameDomain } = pullZone?.data ?? {};
  //
  //   guard({
  //     error: pullZoneError,
  //     condition: !(Id && Name && CnameDomain),
  //     message: 'Unable to add domain',
  //   });
  //
  //   const bunnyValue = `${Name}.${CnameDomain}`;
  //
  //   const pointContent = bunnyValue;
  //
  //   const [mainDnsRecord, mainDnsRecordError] = await resolver(
  //     addDnsRecord({
  //       type: 'CNAME',
  //       name: internal?.name,
  //       content: pointContent,
  //       ttl: 1,
  //     }),
  //   );
  //
  //   console.log({
  //     mainDnsRecord,
  //     mainDnsRecordError,
  //   });
  //
  //   console.log('error-data', mainDnsRecordError?.response);
  //   console.log({ mainDnsRecord, mainDnsRecordError });
  //
  //   guard({ error: mainDnsRecordError, message: 'Unable to add dns record' });
  //
  //   const bunnySite = domains.isReverseProxy
  //     ? internal.domain
  //     : domains.subDomain;
  //
  //   const bunnyAddDomainPromises = [
  //     this.bunnycdnService.addBunnyDomain({
  //       site: bunnySite,
  //       pullZoneId: Id,
  //       setEdgeRule: true,
  //     }),
  //   ];
  //
  //   if (!domains.isReverseProxy) {
  //     bunnyAddDomainPromises.push(
  //       this.bunnycdnService.addBunnyDomain({
  //         site: internal.domain,
  //         pullZoneId: Id,
  //       }),
  //     );
  //   }
  //   await resolver(Promise.all(bunnyAddDomainPromises));
  //
  //   // const [dnsEntry, dnsEntryError] = await resolver(
  //   //   Promise.all([
  //   // this.stackpathService.cloudflareDnsEntry({
  //   //   type: DNS_TYPE.TXT,
  //   //   name: stackpathName,
  //   //   content: stackpath?.domain,
  //   // }),
  //   //     this.stackpathService.cloudflareDnsEntry({
  //   //       type: DNS_TYPE.TXT,
  //   //       name: bunnyName,
  //   //       content: bunnyValue,
  //   //     }),
  //   //   ]),
  //   // );
  //
  //   // console.log({ dnsEntry, dnsEntryError });
  //
  //   // guard({ error: dnsEntryError, message: 'Unable to add cloudflare dns' });
  //
  //   // const [ssl, freeSSLError] = await resolver(
  //   //   this.stackpathService.requestFreeSSL(siteResult?.site.id),
  //   // );
  //
  //   // const freeSSL = ssl?.data;
  //
  //   // guard({ error: freeSSLError, message: 'Unable to get free ssl certicate' });
  //
  //   const [channelCreated, channelCreationError] = await resolver(
  //     this.hostingService.createChannel(
  //       {
  //         user_id: workspace,
  //         site_url: domains.getMainDomain(),
  //         site_proxy_domain: internal?.domain,
  //       },
  //       isOurDomain,
  //       domains.isReverseProxy,
  //     ),
  //   );
  //   guard({ error: channelCreationError, message: 'Unable to create Channel' });
  //
  //   // const { id, status } = siteResult?.site;
  //
  //   const [saveData, saveError] = await resolver(
  //     this.hostingService.updateSiteDB(
  //       { id: channelCreated?.data?.siteId },
  //       {
  //         sp_id: '',
  //         sp_status: 'ACTIVE',
  //         domain_type: type,
  //         certificate_id: '',
  //         is_verified: isOurDomain,
  //         pull_zone_id: Id,
  //         reverseproxy_endpoint: bunnyValue,
  //       },
  //     ),
  //   );
  //
  //   guard({ error: saveError, message: 'Unable to save site in database' });
  //   return sendResponse({
  //     data: {
  //       domain: domains.getMainDomain(),
  //       deliveryDomain: domains?.isReverseProxy ? bunnyValue : internal?.domain,
  //       domainType: type,
  //     },
  //   });
  // }

  @Post('add/v2')
  async addSiteCloudNs(
    @Body() siteData: AddSiteDto,
    @Query() query: AuthQueryDto,
  ) {
    const { workspace } = query;
    const {
      domain = '',
      subDomain = '',
      type = DOMAIN_TYPE.SUBSOMAIN,
    } = siteData ?? {};

    const [, activityError] = await resolver(
      recordActivityApi({
        data: siteData,
        action: 'add-domain',
        ownerId: workspace,
      }),
    );

    if (activityError) {
      console.error(`Error occurred on recording activity - ${activityError}`);
    }

    const [existingSite, existSiteError] = await resolver(
      this.hostingService.getDeletedSiteDB({
        user_id: workspace,
      }),
    );
    if (
      existingSite &&
      existingSite?.created_at &&
      !allowAction(existingSite?.created_at)
    ) {
      guard({
        error:
          'Can only add one domain every 24 hours. Please try again later.',
        message:
          'Can only add one domain every 24 hours. Please try again later.',
        code: 409,
      });
    }

    const [teamIds, teamIdsError] = await resolver(
      this.hostingService.getAllWorkspaceId(workspace),
    );

    if (teamIdsError) console.log(teamIdsError);

    const [activeCount, activeCountError] = await resolver(
      this.hostingService.getActiveSitesCount({
        user_id: In(teamIds),
      }),
    );

    if (activeCountError) {
      console.error(
        `Error occurred while fetching sites count - ${activeCountError}`,
      );
    }

    const [planData, planError] = await resolver(
      planDetailsApi({ userId: workspace }),
    );

    if (planError) {
      console.error(`Error occurred on fetching plan details - ${planError}`);
    }

    const [modData, modError] = await resolver(viewModifiersAPI(workspace));

    if (modError) {
      console.error(
        `Error occurred on fetching modifier details - ${modError}`,
      );
    }

    const customDomains =
      planData?.data?.data?.planConfig?.customDomains ?? null;
    const { data: modifierDetails } = modData?.data;
    const domainMod = modifierDetails?.find((item) => item.type === 'domain');
    const domainLimit = customDomains?.limit ?? 0;
    const domainModCount = domainMod?.quantity ?? 0;
    const totalDomainAfterAddition = activeCount + 1;
    const availableCount = domainLimit + domainModCount;
    // if (customDomains?.access) {
    //   console.error(`Plan don't have an access to add domain`);
    // }

    guard({
      condition: !customDomains?.access,
      message: `Plan don't have an access to add domain`,
    });

    if (
      domainLimit !== 'unlimited' &&
      totalDomainAfterAddition > availableCount
    ) {
      const [, handleModifierError] = await resolver(
        handleModifierApi({ type: 'domain', action: 'add', userId: workspace }),
      );

      if (handleModifierError) {
        console.error(handleModifierError);
      }

      guard({
        error: handleModifierError,
        message: `Error occurred on creating modifier - ${handleModifierError}`,
      });
    }

    const [addCloudNsSite, addCloudNsSiteError] = await resolver(
      this.hostingService.addCloudNsSite(
        workspace,
        domain,
        subDomain,
        type,
        this.awsService,
      ),
    );

    guard({ error: addCloudNsSiteError });
    return addCloudNsSite;
  }

  @Get('add/admin/v2')
  async addSiteCloudNsAdmin(@Query() query: AddSiteV2Dto) {
    const {
      userId: workspace,
      domain = '',
      subDomain = '',
      type = DOMAIN_TYPE.SUBSOMAIN,
      token,
    } = query;

    guard({
      condition: token !== process.env.SERVER_TOKEN,
      code: 403,
      message: 'Forbidden',
    });

    const [, activityError] = await resolver(
      recordActivityApi({
        data: query,
        action: 'add-domain-admin',
        ownerId: workspace,
      }),
    );

    if (activityError) {
      console.error(`Error occurred on recording activity - ${activityError}`);
    }

    const [teamIds, teamIdsError] = await resolver(
      this.hostingService.getAllWorkspaceId(workspace),
    );

    if (teamIdsError) console.log(teamIdsError);

    const [activeCount, activeCountError] = await resolver(
      this.hostingService.getActiveSitesCount({
        user_id: In(teamIds),
      }),
    );

    if (activeCountError) {
      console.error(
        `Error occurred while fetching sites count - ${activeCountError}`,
      );
    }

    const [planData, planError] = await resolver(
      planDetailsApi({ userId: workspace }),
    );

    // guard({ error: planError });
    if (planError) {
      console.error(`Error occurred on fetching plan details - ${planError}`);
    }

    const customDomains =
      planData?.data?.data?.planConfig?.customDomains ?? null;
    const domainLimit = customDomains?.limit ?? 0;
    const totalDomainAfterAddition = activeCount + 1;

    const [addCloudNsSite, addCloudNsSiteError] = await resolver(
      this.hostingService.addCloudNsSite(
        workspace,
        domain,
        subDomain,
        type,
        this.awsService,
      ),
    );

    guard({ error: addCloudNsSiteError });

    if (domainLimit !== 'unlimited' && totalDomainAfterAddition > domainLimit) {
      const [, handleModifierError] = await resolver(
        handleModifierApi({ type: 'domain', action: 'add', userId: workspace }),
      );

      if (handleModifierError) {
        console.error(handleModifierError);
      }

      guard({
        error: handleModifierError,
        message: `Error occurred on creating modifier - ${handleModifierError}`,
      });
    }

    return addCloudNsSite;
  }

  // @Get('add/admin')
  // async addSiteV2(@Query() query: AddSiteV2Dto) {
  //   const {
  //     userId: workspace,
  //     domain = '',
  //     subDomain = '',
  //     type = DOMAIN_TYPE.SUBSOMAIN,
  //     token,
  //   } = query;
  //
  //   guard({
  //     condition: token !== process.env.SERVER_TOKEN,
  //     code: 403,
  //     message: 'Forbidden',
  //   });
  //
  //   const isDomainValid = validate({
  //     domain,
  //     keywords: BLOCKED_DOMAINS,
  //     exact: true,
  //   });
  //
  //   const isSubdomainValid = validate({
  //     domain: subDomain,
  //     keywords: BLOCKED_KEYWORDS,
  //     exact: true,
  //   });
  //
  //   if (type === DOMAIN_TYPE.SUBSOMAIN) {
  //     guard({
  //       condition: !isSubdomainValid,
  //       message: 'Subdomain is not allowed',
  //     });
  //   }
  //
  //   guard({
  //     condition: !isDomainValid,
  //     message: 'Domain is not allowed',
  //   });
  //
  //   // Domain Validation Guards
  //   guard({ condition: !domain, message: 'Domain is required' });
  //
  //   guard({
  //     condition: !subDomain,
  //     message: 'Sub Domain is required',
  //   });
  //
  //   const cleanDomain = cleanURL(domain).toLowerCase();
  //   const cleanSubdomain = cleanURL(subDomain).toLowerCase();
  //
  //   guard({
  //     condition: cleanDomain === cleanSubdomain,
  //     message: 'Domain and Sub Domain should not be same',
  //   });
  //
  //   guard({
  //     condition: cleanSubdomain.includes('.com'),
  //     message: 'Subdomain should not contain ".com"',
  //   });
  //
  //   // Collection of domains and domain related data
  //
  //   // TODO: Convert all the domains to lowercase
  //   const domains = {
  //     subDomain: `${replaceSlash(cleanSubdomain)}.${cleanDomain}`,
  //     reverseProxyDomain: `${cleanDomain}/${cleanSubdomain}`,
  //     isReverseProxy: type === DOMAIN_TYPE.REVERSE_PROXY,
  //
  //     checkIsOurDomain() {
  //       return domain === OUR_DOMAIN && this.isReverseProxy;
  //     },
  //
  //     getMainDomain() {
  //       return this.isReverseProxy ? this.reverseProxyDomain : this.subDomain;
  //     },
  //     getInternalDomain() {
  //       // webstory.link url
  //       return makeInternalDomain(this.subDomain);
  //     },
  //   };
  //
  //   const isOurDomain = domains.checkIsOurDomain();
  //   const internal = domains.getInternalDomain();
  //
  //   console.log({
  //     domains,
  //     isOurDomain,
  //     internal,
  //     mainDomain: domains.getMainDomain(),
  //   });
  //
  //   // Checking if domain Exist
  //   const withHttps = `https://${internal.domain}`;
  //   const [existingDomain, existingDomainError] = await resolver(
  //     this.hostingService.getSiteDB({
  //       site_proxy_domain: withHttps,
  //     }),
  //   );
  //
  //   const condition =
  //     existingDomain?.site_proxy_domain === withHttps &&
  //     existingDomain?.deleted_at === null;
  //
  //   console.log({ existingDomain, existingDomainError, condition, withHttps });
  //   guard({
  //     condition,
  //     message: 'Domain already exist',
  //     code: 409,
  //   });
  //
  //   console.log('after guard');
  //
  //   // const [, tokenError] = await resolver(
  //   //   this.stackpathService.checkTokenValidity(),
  //   // );
  //   // guard({
  //   //   error: tokenError,
  //   //   message: 'Unauthorized',
  //   //   code: HttpStatus.FORBIDDEN,
  //   // });
  //
  //   // const [site, addedSiteError] = await resolver(
  //   //   this.stackpathService.addSite(
  //   //     siteData?.type === DOMAIN_TYPE.SUBSOMAIN
  //   //       ? siteData?.domain
  //   //       : internal?.domain,
  //   //     isOurDomain,
  //   //   ),
  //   // );
  //
  //   // const siteResult = site?.data;
  //
  //   // guard({
  //   //   condition: addedSiteError?.response?.status === 409,
  //   //   message: 'Domain already exist please try another one',
  //   //   code: 409,
  //   // });
  //
  //   // guard({ error: addedSiteError, message: 'Unable to add site' });
  //
  //   // if (siteData?.type === DOMAIN_TYPE.SUBSOMAIN) {
  //   //   const [, deliveryDomainError] = await resolver(
  //   //     this.stackpathService.addDeliveryDomain(
  //   //       internal?.domain,
  //   //       siteResult?.site.id,
  //   //     ),
  //   //   );
  //   //   guard({
  //   //     error: deliveryDomainError,
  //   //     message: 'Unable to add delivery domain',
  //   //   });
  //   // }
  //
  //   // const [domains, domainsError] = await resolver(
  //   //   this.stackpathService.getDeliveryDomains(siteResult.site.id),
  //   // );
  //   // guard({ error: domainsError, message: 'Unable to get delivery domain' });
  //   // const stackpath = domains?.data?.results.find((result: any) =>
  //   //   result?.domain?.includes('stackpathcdn'),
  //   // );
  //
  //   // const stackpathName = `cdn-stackpath-${internal.name}`;
  //
  //   const bunnyName = `cdn-bunny-${internal.name}`;
  //
  //   const [pullZone, pullZoneError] = await resolver(
  //     this.bunnycdnService.addPullZone({ name: internal?.name, isOurDomain }),
  //   );
  //
  //   console.log({ pullZone, pullZoneError });
  //
  //   const { Id, Name, CnameDomain } = pullZone?.data ?? {};
  //
  //   guard({
  //     error: pullZoneError,
  //     condition: !(Id && Name && CnameDomain),
  //     message: 'Unable to add domain',
  //   });
  //
  //   const bunnyValue = `${Name}.${CnameDomain}`;
  //
  //   const pointContent = bunnyValue;
  //
  //   // const pointContent =
  //   //   SELECTED_HOST_TYPE === HOST_TYPE.STACKPATH ? stackpathValue : bunnyValue;
  //
  //   const [mainDnsRecord, mainDnsRecordError] = await resolver(
  //     addDnsRecord({
  //       type: 'CNAME',
  //       name: internal?.name,
  //       content: pointContent,
  //       ttl: 1,
  //     }),
  //   );
  //
  //   console.log('error-data', mainDnsRecordError?.response);
  //   console.log({ mainDnsRecord, mainDnsRecordError });
  //
  //   guard({ error: mainDnsRecordError, message: 'Unable to add dns record' });
  //
  //   const bunnySite = domains.isReverseProxy
  //     ? internal.domain
  //     : domains.subDomain;
  //
  //   const bunnyAddDomainPromises = [
  //     this.bunnycdnService.addBunnyDomain({
  //       site: bunnySite,
  //       pullZoneId: Id,
  //       setEdgeRule: true,
  //     }),
  //   ];
  //
  //   if (!domains.isReverseProxy) {
  //     bunnyAddDomainPromises.push(
  //       this.bunnycdnService.addBunnyDomain({
  //         site: internal.domain,
  //         pullZoneId: Id,
  //       }),
  //     );
  //   }
  //   await resolver(Promise.all(bunnyAddDomainPromises));
  //
  //   // const [dnsEntry, dnsEntryError] = await resolver(
  //   //   Promise.all([
  //   // this.stackpathService.cloudflareDnsEntry({
  //   //   type: DNS_TYPE.TXT,
  //   //   name: stackpathName,
  //   //   content: stackpath?.domain,
  //   // }),
  //   //     this.stackpathService.cloudflareDnsEntry({
  //   //       type: DNS_TYPE.TXT,
  //   //       name: bunnyName,
  //   //       content: bunnyValue,
  //   //     }),
  //   //   ]),
  //   // );
  //
  //   // console.log({ dnsEntry, dnsEntryError });
  //
  //   // guard({ error: dnsEntryError, message: 'Unable to add cloudflare dns' });
  //
  //   // const [ssl, freeSSLError] = await resolver(
  //   //   this.stackpathService.requestFreeSSL(siteResult?.site.id),
  //   // );
  //
  //   // const freeSSL = ssl?.data;
  //
  //   // guard({ error: freeSSLError, message: 'Unable to get free ssl certicate' });
  //
  //   const [channelCreated, channelCreationError] = await resolver(
  //     this.hostingService.createChannel(
  //       {
  //         user_id: workspace,
  //         site_url: domains.getMainDomain(),
  //         site_proxy_domain: internal?.domain,
  //       },
  //       isOurDomain,
  //       domains.isReverseProxy,
  //     ),
  //   );
  //   guard({ error: channelCreationError, message: 'Unable to create Channel' });
  //
  //   // const { id, status } = siteResult?.site;
  //
  //   const [saveData, saveError] = await resolver(
  //     this.hostingService.updateSiteDB(
  //       { id: channelCreated?.data?.siteId },
  //       {
  //         sp_id: '',
  //         sp_status: 'ACTIVE',
  //         domain_type: type,
  //         certificate_id: '',
  //         is_verified: isOurDomain,
  //         pull_zone_id: Id,
  //         reverseproxy_endpoint: bunnyValue,
  //       },
  //     ),
  //   );
  //   guard({ error: saveError, message: 'Unable to save site in database' });
  //   return sendResponse({
  //     data: {
  //       domain: domains.getMainDomain(),
  //       deliveryDomain: domains?.isReverseProxy ? bunnyValue : internal?.domain,
  //       domainType: type,
  //       channelData: channelCreated?.data,
  //     },
  //   });
  // }

  // @Post('cdn/:siteId')
  // async cdnControl(
  //   @Query() query: CdnControlDto,
  //   @Param('siteId') siteId: string,
  // ) {
  //   const [, tokenError] = await resolver(
  //     this.stackpathService.checkTokenValidity(),
  //   );
  //
  //   guard({
  //     error: tokenError,
  //     message: 'Unauthorized',
  //     code: HttpStatus.FORBIDDEN,
  //   });
  //
  //   const { enabled, workspace } = query || {};
  //
  //   const [site, siteError] = await resolver(
  //     // this.hostingService.getSiteDB({ siteId, ownerId: workspace }),
  //     this.hostingService.getSiteDB({ sp_id: siteId, user_id: workspace }),
  //   );
  //
  //   guard({ error: siteError, condition: !site, message: 'Site Not Found' });
  //
  //   if (enabled) {
  //     const [, enableCdnError] = await resolver(
  //       this.stackpathService.cdnControl({ siteId, enabled }),
  //     );
  //     guard({ error: enableCdnError, message: 'Unable to enable cdn' });
  //
  //     const [, updateSiteDBError] = await resolver(
  //       this.hostingService.updateSiteDB(
  //         { sp_id: siteId, user_id: workspace },
  //         { sp_status: 'ACTIVE' },
  //       ),
  //     );
  //
  //     guard({
  //       error: updateSiteDBError,
  //       message: 'Unable to update data in DB',
  //     });
  //
  //     return sendResponse({ message: 'CDN enabled successfully' });
  //   } else {
  //     const [, disableCdnError] = await resolver(
  //       this.stackpathService.cdnControl({ siteId }),
  //     );
  //
  //     guard({ error: disableCdnError, message: 'Unable to disable cdn' });
  //
  //     const [, updateSiteDBError] = await resolver(
  //       this.hostingService.updateSiteDB(
  //         { sp_id: siteId, user_id: workspace },
  //         { sp_status: 'INACTIVE' },
  //       ),
  //     );
  //
  //     guard({
  //       error: updateSiteDBError,
  //       message: 'Unable to update data in DB',
  //     });
  //     return sendResponse({ message: 'CDN disable successfully' });
  //   }
  // }

  // @Get('every')
  // async everySites(
  //   @Query() query: GetEverySitesDto,
  //   @Query() paginationQuery: PaginationQueryDto,
  // ) {
  //   const { startDate, endDate } = query;
  //
  //   const { limit, page } = paginationQuery ?? {};
  //   const [sites, error] = await resolver(
  //     this.hostingService.getSitesPaginated({ limit, page }),
  //   );
  //
  //   guard({ error, message: 'Unable to get domains' });
  //
  //   const [data] = sites;
  //
  //   const [, tokenValidityError] = await resolver(
  //     this.stackpathService.checkTokenValidity(),
  //   );
  //
  //   guard({ error: tokenValidityError, message: 'FORBIDDEN', code: 403 });
  //
  //   const getSitesMetricsPromises =
  //     data?.map((domain: any) => {
  //       return this.stackpathService.getMetrics({
  //         platforms: 'CDE',
  //         granularity: GRANUALARITY.P1D,
  //         sites: domain.sp_id,
  //         start_date: startDate,
  //         end_date: endDate,
  //         group_by: 'SITE',
  //       });
  //     }) ?? [];
  //
  //   const [metrics, metricsError] = await resolver(
  //     Promise.all(getSitesMetricsPromises),
  //   );
  //
  //   //transfer_used_total_mb
  //   const metricsData =
  //     metrics?.map((metric) => {
  //       const metricData = metric?.data?.data?.matrix?.results;
  //       const totalUsageMetric = metricData.find(
  //         (data) => data.metric.__name__ === 'transfer_used_total_mb',
  //       );
  //
  //       const siteId = totalUsageMetric?.metric?.site_id;
  //
  //       const reducer = (accumulator: any, current: any) =>
  //         accumulator + Number(current.value);
  //
  //       const site = data?.find((domain: any) => domain.sp_id === siteId);
  //       const total = totalUsageMetric?.values?.reduce(reducer, 0) ?? 0;
  //       return {
  //         ...site,
  //         usageInMB: total,
  //       };
  //     }) ?? [];
  //
  //   // Take out bunny statistics
  //   // Bunny give "TotalBandwidthUsed" in bytes
  //
  //   guard({ error: metricsError, message: 'Unable to get sites bandwidth' });
  //
  //   return sendResponse({ data: metricsData });
  // }

  // @Get('usage')
  // async getUsage(
  //   @Query() query: GetEverySitesDto,
  //   @Query() paginationQuery: PaginationQueryDto,
  // ) {
  //   const { startDate, endDate } = query ?? {};
  //   const { page } = paginationQuery ?? {};
  //
  //   const [, tokenValidityError] = await resolver(
  //     this.stackpathService.checkTokenValidity(),
  //   );
  //
  //   const [sitesData, error] = await resolver(
  //     this.hostingService.getSitesPaginated({ limit: 1, page }),
  //   );
  //
  //   const [sites, count] = sitesData;
  //   const [site] = sites;
  //
  //   guard({ error, condition: !site, message: 'Unable to get domains' });
  //
  //   const { sp_id, pull_zone_id } = site ?? {};
  //
  //   const bandwidthPromises = [];
  //
  //   // get stackpath bandwidth usage
  //   if (sp_id) {
  //     guard({ error: tokenValidityError, message: 'FORBIDDEN', code: 403 });
  //     bandwidthPromises[0] = this.stackpathService.getMetrics({
  //       platforms: 'CDE',
  //       granularity: GRANUALARITY.P1D,
  //       sites: sp_id,
  //       start_date: startDate,
  //       end_date: endDate,
  //       group_by: 'SITE',
  //     });
  //   }
  //
  //   let usageInMb = 0;
  //
  //   if (pull_zone_id) {
  //     bandwidthPromises[1] = this.bunnycdnService.getStatistics({
  //       pullZone: pull_zone_id,
  //       dateFrom: startDate,
  //       dateTo: endDate,
  //     });
  //   }
  //
  //   if (!bandwidthPromises?.length) {
  //     return sendResponse({ data: { ...site, usageInMb, count } });
  //   }
  //
  //   const [promiseData, promiseError] = await resolver(
  //     Promise.all(bandwidthPromises),
  //   );
  //
  //   console.log(promiseError);
  //   guard({ error: promiseError, message: 'Unable to get bandwidth usage' });
  //
  //   const [stackpathMetric, bunnyMetric] = promiseData;
  //
  //   if (stackpathMetric) {
  //     usageInMb += this.stackpathService.getUsageFromMetrics(stackpathMetric);
  //   }
  //
  //   if (bunnyMetric) {
  //     const inBytes = bunnyMetric?.data?.TotalBandwidthUsed;
  //
  //     usageInMb += inBytes / 1000 / 1000;
  //   }
  //
  //   return sendResponse({ data: { ...site, usageInMb, count } });
  // }

  @Get('all')
  async getSites(
    @Query() query: GetSitesDto,
    @Query() paginationQuery: PaginationQueryDto,
  ) {
    const {
      workspace,
      // includeBandwidth,
      // startDate,
      // endDate
    } = query;
    const { limit, page } = paginationQuery ?? {};
    const [sites, error] = await resolver(
      this.hostingService.getSitesPaginated(
        { limit, page },
        { user_id: workspace },
      ),
    );
    const [data, count] = sites ?? [];
    guard({ error, message: 'Unable to get connected domains' });

    const withOurdomain =
      data?.map((item) => {
        const showReverseProxyDomain =
          item?.domain_type === 'REVERSE_PROXY' && item?.reverseproxy_endpoint;
        return {
          ...item,
          is_our_domain: checkOurDomain(item?.site_url),
          cname_name: removeHttps(item?.site_url),
          cname_value: showReverseProxyDomain
            ? item?.reverseproxy_endpoint
            : removeHttps(item?.site_proxy_domain),
        };
      }) ?? [];

    // if (!includeBandwidth) {
    const paginatedData = withPaginationMetadata({
      fieldName: 'list',
      data: withOurdomain,
      count,
      limit,
      page,
    });
    return sendResponse({ data: paginatedData });
    // }

    // const [, tokenValidityError] = await resolver(
    //   this.stackpathService.checkTokenValidity(),
    // );
    //
    // guard({ error: tokenValidityError, message: 'FORBIDDEN', code: 403 });
    //
    // const getSitesMetricsPromises =
    //   data?.map(({ sp_id }: any) => {
    //     return this.stackpathService.getMetrics({
    //       platforms: 'CDE',
    //       granularity: GRANUALARITY.P1D,
    //       sites: sp_id,
    //       start_date: startDate,
    //       end_date: endDate,
    //       group_by: 'SITE',
    //     });
    //   }) ?? [];
    //
    // const [metrics, metricsError] = await resolver(
    //   Promise.all(getSitesMetricsPromises),
    // );
    //
    // //transfer_used_total_mb
    // const metricsData =
    //   metrics?.map((metric) => {
    //     const metricData = metric?.data?.data?.matrix?.results;
    //     const totalUsageMetric = metricData.find(
    //       (data) => data.metric.__name__ === 'transfer_used_total_mb',
    //     );
    //
    //     const siteId = totalUsageMetric?.metric?.site_id;
    //
    //     const reducer = (accumulator, current) =>
    //       accumulator + Number(current.value);
    //
    //     const site = data?.find((domain) => domain.sp_id === siteId);
    //     const total = totalUsageMetric?.values?.reduce(reducer, 0) ?? 0;
    //     return {
    //       ...site,
    //       usageInMB: total,
    //     };
    //   }) ?? [];
    //
    // guard({ error: metricsError, message: 'Unable to get sites bandwidth' });
    // return sendResponse({ data: metricsData });
  }

  @Get('all/admin')
  async getSitesAdmin(
    @Query() query: GetAllSiteAdminDto,
    @Query() paginationQuery: PaginationQueryDto,
  ) {
    const { token = '', ownerId = '' } = query ?? {};

    guard({
      condition: token !== process.env.SERVER_TOKEN,
      code: 403,
      message: 'Forbidden',
    });

    const { limit, page, search = '' } = paginationQuery ?? {};

    const siteQuery = {};

    if (search) {
      siteQuery['site_url'] = Like(`%${search}%`);
    }

    if (ownerId) {
      const [teamIds] = await resolver(
        this.hostingService.getAllWorkspaceId(ownerId),
      );
      siteQuery['user_id'] = In([teamIds]);
    }
    const [sites, error] = await resolver(
      this.hostingService.getAllSitesPaginated({ limit, page }, siteQuery),
    );

    const [data, count] = sites ?? [];
    guard({ error, message: 'Unable to get connected domains' });

    const withOurdomain =
      data?.map((item) => {
        const showReverseProxyDomain =
          item?.domain_type === 'REVERSE_PROXY' && item?.reverseproxy_endpoint;
        return {
          ...item,
          is_our_domain: checkOurDomain(item?.site_url),
          cname_name: removeHttps(item?.site_url),
          cname_value: showReverseProxyDomain
            ? item?.reverseproxy_endpoint
            : removeHttps(item?.site_proxy_domain),
        };
      }) ?? [];

    const paginatedData = withPaginationMetadata({
      fieldName: 'list',
      data: withOurdomain,
      count,
      limit,
      page,
    });

    return sendResponse({ data: paginatedData });
  }

  // @Post('verify/:id')
  // async checkVerification(
  //   @Query() authQuery: AuthQueryDto,
  //   @Param('id') id: string,
  // ) {
  //   const [site, siteError] = await resolver(
  //     this.hostingService.getSiteDB({
  //       id: Number(id),
  //       user_id: authQuery.workspace,
  //     }),
  //   );
  //
  //   guard({ error: siteError, message: 'Unable to verify site' });
  //   guard({ condition: !site, message: 'Site not found' });
  //
  //   const [sslCertificate] = await resolver(
  //     this.hostingService.requestFreeSSLCertificate(site),
  //   );
  //
  //   guard({ condition: !sslCertificate, message: 'Site is not yet verified' });
  //
  //   const [siteVerfied] = await resolver(
  //     this.hostingService.requestVerification(site),
  //   );
  //
  //   guard({ condition: !siteVerfied, message: 'Site is not yet verified' });
  //
  //   return sendResponse({ message: 'Site is verified' });
  // }

  @Post('verify/v2/:id')
  async checkVerificationCouldNs(
    @Query() authQuery: AuthQueryDto,
    @Param('id') id: string,
  ) {
    const [site, siteError] = await resolver(
      this.hostingService.getSiteDB({
        id: id,
        user_id: authQuery.workspace,
      }),
    );
    guard({ error: siteError, message: 'Site not found' });
    guard({ condition: !site, message: 'Site not found' });
    const [, verifyDnsError] = await resolver(
      this.hostingService.verifyCloudNs(site, this.awsService),
    );
    console.log('verifyDnsError:', verifyDnsError);
    guard({
      error: verifyDnsError?.response?.status || verifyDnsError,
      message: verifyDnsError?.response?.message || 'Failed to verify Site',
    });
    return sendResponse({ message: 'Site is verified' });
    // const domain = getDomain(`${site?.site_url}/`);
    // const [CertificateDetails, error] = await resolver(
    //   this.awsService.getCertificateDetails({
    //     certificateKey: site?.certificate_id,
    //   }),
    // );
    // guard({
    //   error: error,
    //   message: 'Unable fetch certificate',
    // });
    // const Certificate = CertificateDetails?.Certificate;
    // console.log(Certificate);
    // if (Certificate.Status === 'ISSUED') {
    //   const [cloudFrontData, cloudFrontError] = await resolver(
    //     this.awsService.createDistribution({
    //       certificateARN: site?.certificate_id,
    //       domain: domain,
    //     }),
    //   );
    //   console.log(cloudFrontError);
    //   guard({
    //     error: cloudFrontError,
    //     message: 'Unable to create distribution',
    //   });
    //   const [cloudDnsData, cloudDnserror] = await resolver(
    //     addCloudNsRecord({
    //       'record-type': 'CNAME',
    //       'domain-name': INTERNAL_DOMAIN,
    //       host: makeInternalDomain(domain).name,
    //       record: cloudFrontData.Distribution.DomainName,
    //       ttl: 60,
    //       proxied: false,
    //     }),
    //   );
    //   console.log({ cloudDnsData, cloudDnserror });
    //   guard({ error: cloudDnserror, message: 'Unable to add dns record' });
    //   if (cloudDnsData?.data?.status === 'Failed') {
    //     guard({
    //       error: cloudDnsData?.data?.statusDescription,
    //       message: 'Unable to add dns record',
    //     });
    //   }
    //   //update user_site
    //   const [, updateErr] = await resolver(
    //     this.hostingService.updateSiteDB(
    //       { id: id },
    //       {
    //         is_verified: true,
    //         is_ssl_verified: true,
    //         cf_distribution_id: cloudFrontData.Id,
    //         dns_id: cloudDnsData?.data?.data?.id,
    //       },
    //     ),
    //   );
    //   console.log(updateErr);
    //   return sendResponse({ message: 'Site is verified' });
    // }
    // if (Certificate.Status === 'FAILED') {
    //   const [, delErr] = await resolver(
    //     this.awsService.deleteCertificateDetails({
    //       certificateKey: site?.certificate_id,
    //     }),
    //   );
    //   console.error(delErr);
    //   guard({
    //     error: delErr,
    //     message:
    //       'Site Validation Failed, Please delete and re-create your domain',
    //   });
    // }
    // guard({
    //   error: 'PENDING_VALIDATION',
    //   message: 'Dns validation in progress, Please check back in 30 minutes',
    // });
  }

  // @Get('verify/admin/:id')
  // async verifyV2(@Query('token') token: string, @Param('id') id: string) {
  //   guard({
  //     condition: token !== process.env.SERVER_TOKEN,
  //     code: 403,
  //     message: 'Forbidden',
  //   });
  //   const [site, siteError] = await resolver(
  //     this.hostingService.getSiteDB({
  //       id: Number(id),
  //     }),
  //   );
  //
  //   guard({ error: siteError, message: 'Unable to verify site' });
  //   guard({ condition: !site, message: 'Site not found' });
  //
  //   const [sslCertificate] = await resolver(
  //     this.hostingService.requestFreeSSLCertificate(site),
  //   );
  //
  //   guard({ condition: !sslCertificate, message: 'Site is not yet verified' });
  //
  //   const [siteVerfied] = await resolver(
  //     this.hostingService.requestVerification(site),
  //   );
  //
  //   guard({ condition: !siteVerfied, message: 'Site is not yet verified' });
  //
  //   return sendResponse({ message: 'Site is verified' });
  // }

  @Get('verify/admin/v2/:id')
  async verifyAdminV2(@Query('token') token: string, @Param('id') id: string) {
    guard({
      condition: token !== process.env.SERVER_TOKEN,
      code: 403,
      message: 'Forbidden',
    });
    const [site, siteError] = await resolver(
      this.hostingService.getSiteDB({
        id: Number(id),
      }),
    );
    guard({ error: siteError, message: 'Unable to verify site' });
    guard({ condition: !site, message: 'Site not found' });
    const [, verifyDnsError] = await resolver(
      this.hostingService.verifyCloudNs(site, this.awsService),
    );
    console.log('verifyDnsError:', verifyDnsError);
    guard({
      error: verifyDnsError?.response?.status || verifyDnsError,
      message: verifyDnsError?.response?.message || 'Failed to verify Site',
    });
    return sendResponse({ message: 'Site is verified' });
  }

  // @Post('ssl/:id')
  // async requestSSL(@Query() authQuery: AuthQueryDto, @Param('id') id: string) {
  //   const [site, siteError] = await resolver(
  //     this.hostingService.getSiteDB({
  //       id: Number(id),
  //       user_id: authQuery.workspace,
  //     }),
  //   );
  //
  //   console.log({ site, siteError });
  //
  //   guard({
  //     error: siteError,
  //     condition: !site,
  //     message: 'Site not found',
  //     code: 404,
  //   });
  //
  //   const [sslCertificate] = await resolver(
  //     this.hostingService.requestFreeSSLCertificate(site),
  //   );
  //
  //   guard({
  //     error: !sslCertificate,
  //     message:
  //       'Unable to get ssl certificate. Please verify your domain first.',
  //   });
  //
  //   return sendResponse({ message: 'Site is ssl verified' });
  // }

  // @Get('restore/admin/:id')
  // async restoreSite(@Param('id') id: string, @Query('token') token: string) {
  //   guard({
  //     condition: token !== process.env.SERVER_TOKEN,
  //     code: 403,
  //     message: 'Forbidden',
  //   });
  //
  //   const [site, siteError] = await resolver(
  //     this.hostingService.findOne({
  //       id: Number(id),
  //     }),
  //   );
  //
  //   guard({ error: siteError, message: 'Unable to restore Site' });
  //   guard({ condition: !site?.deleted_at, message: 'Site is not deleted' });
  //
  //   const siteUrl = removeHttps(site?.site_url);
  //   const siteProxyDomain = removeHttps(site?.site_proxy_domain);
  //
  //   const isReverseProxy = site?.domain_type === 'REVERSE_PROXY';
  //   const isOurDomain = isReverseProxy && checkOurDomain(siteUrl);
  //
  //   const zoneId = identifyZoneID(siteProxyDomain);
  //   const cname = removeInternalDomain(siteProxyDomain);
  //
  //   //! ADDING PULLZONE
  //   const [pullZone, pullZoneError] = await resolver(
  //     this.bunnycdnService.addPullZone({
  //       name: cname,
  //       isOurDomain,
  //     }),
  //   );
  //
  //   const { Id, Name, CnameDomain } = pullZone?.data ?? {};
  //
  //   guard({
  //     error: pullZoneError,
  //     condition: !(Id && Name && CnameDomain),
  //     message: 'Unable to add domain',
  //   });
  //
  //   const pointContent = `${Name}.${CnameDomain}`;
  //
  //   // ! ADDING DNS RECORD
  //   const [mainDnsRecord, mainDnsRecordError] = await resolver(
  //     addDnsRecord({
  //       type: 'CNAME',
  //       name: cname,
  //       content: pointContent,
  //       ttl: 1,
  //       zone: zoneId,
  //     }),
  //   );
  //
  //   guard({ error: mainDnsRecordError, message: 'Unable to add dns record' });
  //
  //   const bunnyAddDomainPromises = [
  //     this.bunnycdnService.addBunnyDomain({
  //       site: site?.site_url,
  //       pullZoneId: Id,
  //       setEdgeRule: true,
  //     }),
  //   ];
  //
  //   if (!isReverseProxy) {
  //     bunnyAddDomainPromises.push(
  //       this.bunnycdnService.addBunnyDomain({
  //         site: siteProxyDomain,
  //         pullZoneId: Id,
  //       }),
  //     );
  //   }
  //   await resolver(Promise.all(bunnyAddDomainPromises));
  //
  //   //! UPDATE DATABASE
  //   const [saveData, saveError] = await resolver(
  //     this.hostingService.updateSiteDBv2(
  //       { id: site?.id },
  //       {
  //         pull_zone_id: Id,
  //         reverseproxy_endpoint: pointContent,
  //         deleted_at: null,
  //       },
  //     ),
  //   );
  //
  //   guard({ error: saveError, message: 'Unable to save site in database' });
  //   return sendResponse({
  //     message: `Site: ${site?.id} Restored Successfully`,
  //   });
  // }

  @Delete('remove/:id')
  async removeDomain(@Param('id') id: string, @Query() query: AuthQueryDto) {
    const [site, siteError] = await resolver(
      this.hostingService.getSiteDB({
        id: Number(id),
        user_id: query.workspace,
      }),
    );

    guard({
      error: siteError,
      condition: !site,
      message: 'Site not found',
      code: 404,
    });

    const [, activityError] = await resolver(
      recordActivityApi({
        data: { id },
        action: 'remove-domain',
        ownerId: query?.workspace,
      }),
    );

    if (activityError) {
      console.error(`Error occurred on recording activity - ${activityError}`);
    }

    if (site && site?.created_at && !allowAction(site?.created_at)) {
      guard({
        error: 'Cannot delete a domain that was added in the past 24 hours.',
        message: 'Cannot delete a domain that was added in the past 24 hours.',
        code: 409,
      });
    }

    const [, removeDomainError] = await resolver(
      this.hostingService.removeDomain(site, this.awsService),
    );
    console.error('Error occurred while removing domain: ', removeDomainError);
    guard({ error: removeDomainError });

    const [planData, planError] = await resolver(
      planDetailsApi({ userId: query?.workspace }),
    );

    // guard({ error: planError });
    if (planError) {
      console.error(`Error occurred on getting plan details - ${planError}`);
    }

    const [teamIds, teamIdsError] = await resolver(
      this.hostingService.getAllWorkspaceId(query?.workspace),
    );

    if (teamIdsError) console.log(teamIdsError);

    const [activeCount, activeCountError] = await resolver(
      this.hostingService.getActiveSitesCount({
        user_id: In(teamIds),
      }),
    );

    // guard({error: activeCountError})
    if (activeCountError) {
      console.error(
        `Error occurred while fetching sites count - ${activeCountError}`,
      );
    }

    const customDomains =
      planData?.data?.data?.planConfig?.customDomains ?? null;
    const domainLimit = customDomains?.limit ?? 0;

    if (domainLimit !== 'unlimited' && activeCount >= domainLimit) {
      const [, handleModifierError] = await resolver(
        handleModifierApi({
          type: 'domain',
          action: 'remove',
          userId: query?.workspace,
        }),
      );

      guard({
        error: handleModifierError,
        message: `Error occurred on removing modifier - ${handleModifierError}`,
      });
      // if (handleModifierError) {
      //   console.error(
      //     `Error occurred on handling modifier - ${handleModifierError}`,
      //   );
      // }
    }

    return sendResponse({ message: 'Domain removed' });
  }

  @Get('remove/admin/:id')
  async removeDomainV2(@Param('id') id: string, @Query('token') token: string) {
    guard({
      condition: token !== process.env.SERVER_TOKEN,
      code: 403,
      message: 'Forbidden',
    });

    const [site, siteError] = await resolver(
      this.hostingService.getSiteDB({
        id: Number(id),
      }),
    );

    guard({
      error: siteError,
      condition: !site,
      message: 'Site not found',
      code: 404,
    });

    const [, activityError] = await resolver(
      recordActivityApi({
        data: { id },
        action: 'remove-domain-admin',
        ownerId: site?.user_id,
      }),
    );

    if (activityError) {
      console.error(`Error occurred on recording activity - ${activityError}`);
    }

    const [, removeDomainError] = await resolver(
      this.hostingService.removeDomain(site, this.awsService),
    );

    guard({ error: removeDomainError });

    const [planData, planError] = await resolver(
      planDetailsApi({ userId: site?.user_id }),
    );

    // guard({ error: planError });
    if (planError) {
      console.error(`Error occurred on getting plan details - ${planError}`);
    }

    const [teamIds, teamIdsError] = await resolver(
      this.hostingService.getAllWorkspaceId(site?.user_id),
    );

    if (teamIdsError) console.log(teamIdsError);

    const [activeCount, activeCountError] = await resolver(
      this.hostingService.getActiveSitesCount({
        user_id: In(teamIds),
      }),
    );

    // guard({error: activeCountError})
    if (activeCountError) {
      console.error(
        `Error occurred while fetching sites count - ${activeCountError}`,
      );
    }

    const customDomains =
      planData?.data?.data?.planConfig?.customDomains ?? null;
    const domainLimit = customDomains?.limit ?? 0;

    if (domainLimit !== 'unlimited' && activeCount >= domainLimit) {
      const [, handleModifierError] = await resolver(
        handleModifierApi({
          type: 'domain',
          action: 'remove',
          userId: site?.user_id,
        }),
      );

      guard({
        error: handleModifierError,
        message: `Error occurred on removing modifier - ${handleModifierError}`,
      });
    }

    return sendResponse({ message: 'Domain removed' });
  }

  @Get('autodelete')
  async autoDelete(@Query('token') token: string) {
    unlinkSync('delete.txt');
    unlinkSync('warning.txt');
    unlinkSync('log.txt');

    guard({
      condition: token !== process.env.SERVER_TOKEN,
      code: 403,
      message: 'Forbidden',
    });

    writeFileSync('delete.txt', '');
    writeFileSync('warning.txt', '');
    writeFileSync('log.txt', '');
    const [deleted, error] = await resolver(
      this.hostingService.removeDomainCRON(),
    );

    guard({ error });
    return sendResponse({ message: 'Auto delete completed' });
  }

  @Get('/update/distribution')
  async updateDistribution() {
    return await this.hostingService.updateDistribution();
  }

  @Get('/invalidate-distribution/wp/:siteId')
  async invalidateDistributionWp(
    @Param('siteId') siteId: string,
    @Query('token') token: string,
  ) {
    guard({
      condition: token !== process.env.SERVER_TOKEN,
      code: 403,
      message: 'Forbidden',
    });
    return await this.hostingService.invalidateDistribution('wp/' + siteId);
  }

  @Get('/invalidate-distribution/:siteId')
  async invalidateDistribution(
    @Param('siteId') siteId: string,
    @Query('token') token: string,
  ) {
    guard({
      condition: token !== process.env.SERVER_TOKEN,
      code: 403,
      message: 'Forbidden',
    });
    return await this.hostingService.invalidateDistribution(siteId);
  }

  /* CRON JOBS */
  // @Cron(CronExpression.EVERY_DAY_AT_MIDNIGHT)
  async removeDomainCRON() {
    await this.hostingService.removeDomainCRON();
    return true;
  }

  // @Cron(CronExpression.EVERY_10_MINUTES)
  async removeDomainDistribution() {
    await this.hostingService.removeDomainDistribution();
    return true;
  }

  // @Cron(CronExpression.EVERY_30_MINUTES)
  async requestVerificationCron() {
    await this.hostingService.requestVerificationCron();
    return true;
  }
}
