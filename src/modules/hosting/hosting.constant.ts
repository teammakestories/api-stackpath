export const DOMAIN_TYPE = Object.freeze({
  SUBSOMAIN: 'SUBDOMAIN',
  REVERSE_PROXY: 'REVERSE_PROXY',
});

export const DNS_TYPE = Object.freeze({
  CNAME: 'CNAME',
  TXT: 'TXT',
});

export const OUR_DOMAIN = 'stories.site';

export const BLOCKED_DOMAINS = [
  'blogspot.com',
  'blogger.com',
  'wix.com',
  'blogspot',
  'blogspots',
];

export const BLOCKED_KEYWORDS = ['www'];

export const ALLOWED_FIELDS_TO_UPDATE = new Set([
  'is_verified',
  'is_ssl_verified',
  'lp_status',
]);
