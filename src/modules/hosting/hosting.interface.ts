export interface SiteInterface {
  id?: any;
  user_id?: string;
  site_url?: string;
  channel_id?: string;
  site_proxy_domain?: string;
  cache_clear_webhook?: string;
  callback_webhook?: string;
  sp_id?: string;
  sp_status?: string;
  domain_type?: string;
  certificate_id?: string;
  is_verified?: boolean;
  warning_sent?: any;
  is_ssl_verified?: boolean;
  pull_zone_id?: string;
  created_at?: string;
  deleted_at?: string;
  lp_status?: boolean;
  reverseproxy_endpoint?: string;
  cf_distribution_id?: string;
  dns_id?: string;
  ssl_dns_id?: string;
  ssl_cname_record?: string;
  ssl_cname_value?: string;
}

export interface CreateChannelInterface {
  userId: string;
  domain: string;
  proxy: string;
  isOurDomain: boolean;
}

