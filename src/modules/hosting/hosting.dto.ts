import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import {
  IsBoolean,
  IsInt,
  IsNotEmpty,
  IsOptional,
  IsString,
} from 'class-validator';
import { AuthQueryDto, ToBoolean } from 'nest-library';

export type DomainType = 'SUBDOMAIN' | 'REVERSE_PROXY';
export class AddSiteDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ required: true })
  domain: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ required: true })
  subDomain: string;

  // @IsString()
  // @IsOptional()
  // @ApiProperty({
  //   required: false,
  //   description: 'only applicable for reverse proxy | eg: makestories.io/demo',
  // })
  // @IsString()
  // reverseProxyDomain: string;

  @IsNotEmpty()
  @ApiProperty({ required: true, description: 'SUBDOMAIN | REVERSE_PROXY' })
  type: DomainType;
}

export class GetSitesDto extends AuthQueryDto {
  // @IsOptional()
  // @IsBoolean()
  // @ToBoolean()
  // @ApiProperty({ required: false, description: 'Retrieve bandwidth data' })
  // includeBandwidth: boolean;

  // @IsOptional()
  // @IsString()
  // @ApiProperty({
  //   required: false,
  //   description:
  //     'If includeBandwith=true, startDate is required | Format: 2021-06-01T00:00:00Z',
  // })
  // startDate: string;

  // @IsOptional()
  // @IsString()
  // @ApiProperty({
  //   required: false,
  //   description:
  //     'If includeBandwith=true, endDate is required | Format: 2021-06-01T00:00:00Z',
  // })
  // endDate: string;
}

export class GetEverySitesDto {
  @IsOptional()
  @IsString()
  @ApiProperty({
    required: false,
    description: 'startDate is required | Format: 2021-06-01T00:00:00Z',
  })
  startDate: string;

  @IsOptional()
  @IsString()
  @ApiProperty({
    required: false,
    description: 'endDate is required | Format: 2021-06-01T00:00:00Z',
  })
  endDate: string;
}

export class CdnControlDto extends AuthQueryDto {
  @ToBoolean()
  @IsBoolean()
  @IsNotEmpty()
  @ApiProperty({ type: Boolean })
  enabled: boolean;
}

export class AddSiteV2Dto {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  token: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  userId: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  domain: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  subDomain: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  type: DomainType;
}

export class GetAllSiteAdminDto {
  @IsOptional()
  @IsString()
  @ApiProperty({ required: false })
  ownerId?: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  token: string;
}

export class EditSiteDto {
  @IsOptional()
  @IsString()
  @ApiProperty({ required: false, description: 'NOT ALLOWED' })
  user_id?: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ required: false, description: 'NOT ALLOWED' })
  site_url?: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ required: false, description: 'NOT ALLOWED' })
  channel_id?: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ required: false, description: 'NOT ALLOWED' })
  site_proxy_domain?: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ required: false, description: 'NOT ALLOWED' })
  cache_clear_webhook?: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ required: false, description: 'NOT ALLOWED' })
  callback_webhook?: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ required: false, description: 'NOT ALLOWED' })
  sp_id?: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ required: false, description: 'NOT ALLOWED' })
  sp_status?: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ required: false, description: 'NOT ALLOWED' })
  domain_type?: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ required: false, description: 'NOT ALLOWED' })
  certificate_id?: string;

  @IsOptional()
  @IsBoolean()
  @ApiProperty({ required: false, description: 'ALLOWED' })
  is_verified?: boolean;

  @IsOptional()
  @IsBoolean()
  @ApiProperty({ required: false, description: 'ALLOWED' })
  is_ssl_verified?: boolean;

  @IsOptional()
  @IsString()
  @ApiProperty({ required: false, description: 'NOT ALLOWED' })
  pull_zone_id?: string;

  @IsOptional()
  @IsBoolean()
  @ApiProperty({ required: false, description: 'ALLOWED' })
  lp_status?: boolean;
}
