import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import axios from 'axios';
import {
  guardV2,
  // resolveAll,
  resolver,
  sleep,
} from 'nest-library';
import { deleteDnsRecord } from 'src/api/cloudflare';
import {
  // makestoriesApiBaseUrl,
  makestoriesNodeApiBaseUrl,
} from 'src/config/app.config';
import { HOSTING_SETUP_URL } from 'src/constants/api.constant';
import {
  cleanURL,
  getDomain,
  guard,
  makeInternalDomain,
  replaceSlash,
  sendResponse,
} from 'src/utils/common.util';
import { IsNull, Repository, Not, Between } from 'typeorm';
import { BunnycdnService } from '../bunnycdn/bunnycdn.service';
import { StackpathService } from '../stackpath/stackpath.service';
import {
  BLOCKED_DOMAINS,
  BLOCKED_KEYWORDS,
  DOMAIN_TYPE,
  OUR_DOMAIN,
} from './hosting.constant';
import { SiteInterface } from './hosting.interface';
import { UserSites } from './site.entity';
import { appendFileSync } from 'fs';
import {
  identifyDomainType,
  identifyZoneID,
} from 'src/api/cloudflare/cloudflare.util';
import {
  createStoryListingAPI,
  generateSiteMap,
  getStoriesCountAPI,
  sendWarningEmail,
} from 'src/api/domain';
import { FTPDetails } from './ftpdetails.entity';
import { addCloudNsRecord, deleteClouDnsRecord } from '../cloudns';
import { AwsService } from '../aws/aws.service';
import { CURRENT_DOMAIN_CONFIG } from 'src/config/app.config';
import { validate } from './hosting.util';
import {
  checkModifierApi,
  handleModifierApi,
  planDetailsApi,
} from 'src/api/billingApis';
import { Users } from 'makestories-library';
import { Teams } from 'makestories-library';

const { INTERNAL_DOMAIN, ARN } = CURRENT_DOMAIN_CONFIG;

@Injectable()
export class HostingService {
  running = false;
  constructor(
    @InjectRepository(UserSites)
    private userSitesModel: Repository<UserSites>,

    @InjectRepository(FTPDetails)
    private ftpSiteModel: Repository<FTPDetails>,

    @InjectRepository(Users)
    private usersModel: Repository<Users>,

    @InjectRepository(Teams)
    private teamsModel: Repository<Teams>,

    private readonly bunnycdnService: BunnycdnService,
    private readonly stackpathService: StackpathService,
    private readonly awsCouldNsService: AwsService,
  ) {}

  saveSiteDB(data: SiteInterface) {
    return this.userSitesModel.create(data);
  }

  getFTPDetail(findQuery: any) {
    console.log({ findQuery });
    return this.ftpSiteModel.findOne({ where: findQuery });
  }

  async updateSiteDB(query: any, data: SiteInterface) {
    const [site, error] = await resolver(this.getSiteDB(query));
    guardV2({ error });
    for (const key in data) {
      site[key] = data[key];
    }
    return this.userSitesModel.save(site);
  }

  async updateDeletedSiteDB(query: any, data: SiteInterface) {
    const [site, error] = await resolver(this.getDeletedSiteDB(query));
    guardV2({ error });
    for (const key in data) {
      site[key] = data[key];
    }
    return this.userSitesModel.save(site);
  }

  getSiteDB(findQuery: any) {
    return this.userSitesModel.findOne({
      where: { ...findQuery, deleted_at: null },
    });
  }

  getDeletedSiteDB(findQuery: any) {
    return this.userSitesModel.findOne({
      where: { ...findQuery },
      withDeleted: true,
      order: { created_at: 'DESC' },
    });
  }

  findOne(findQuery: any) {
    return this.userSitesModel.findOne({
      where: findQuery,
      withDeleted: true,
    });
  }

  async updateSiteDBv2(query: any, data: SiteInterface) {
    const [site, error] = await resolver(this.findOne(query));
    guardV2({ error });
    for (const key in data) {
      site[key] = data[key];
    }
    return this.userSitesModel.save(site);
  }

  getSitesDB(query: any) {
    return this.userSitesModel.find({ where: { ...query, deleted_at: null } });
  }

  getSitesPaginated(
    { limit = 10, page = 1 } = { limit: 10, page: 1 },
    query: any = {},
  ) {
    const skip = (page - 1) * limit;
    return this.userSitesModel.findAndCount({
      where: { ...query, deleted_at: null },
      take: limit,
      order: { created_at: 'DESC' },
      skip,
    });
  }

  getAllSitesPaginated(
    { limit = 10, page = 1 } = { limit: 10, page: 1 },
    query: any = {},
  ) {
    const skip = (page - 1) * limit;
    return this.userSitesModel.findAndCount({
      where: { ...query },
      withDeleted: true,
      take: limit,
      order: { created_at: 'DESC' },
      skip,
    });
  }

  getDeletedSites(query: any) {
    return this.userSitesModel.find({
      where: { ...query, deleted_at: Not(IsNull()) },
      withDeleted: true,
      take: 5,
      order: { created_at: 'DESC' },
    });
  }

  getAllSitesCount(query: any) {
    return this.userSitesModel.count({
      where: { ...query },
      withDeleted: true,
    });
  }

  getActiveSitesCount(query: any) {
    return this.userSitesModel.count({
      where: { ...query },
    });
  }

  createChannel(
    { user_id, site_url, site_proxy_domain }: SiteInterface,
    isOurDomain = false,
    isReverseProxy = false,
  ) {
    const ftpFolder = isOurDomain ? site_url.replace('stories.site/', '') : '';
    const basePath = isReverseProxy ? site_proxy_domain : site_url;

    const ourDomainFTPConfig = {
      host: process.env.FTP_HOSTNAME_2,
      port: process.env.FTP_PORT_2,
      username: process.env.FTP_USERNAME_2,
      password: process.env.FTP_PASSWORD_2,
      base_path: `${process.env.FTP_BASEPATH_2}/${ftpFolder}`,
    };

    const defaultFTPConfig = {
      host: process.env.FTP_HOSTNAME,
      port: process.env.FTP_PORT,
      username: process.env.FTP_USERNAME,
      password: process.env.FTP_PASSWORD,
      base_path: `${process.env.FTP_BASEPATH}/${basePath}`,
    };

    const ftpConfig = isOurDomain ? ourDomainFTPConfig : defaultFTPConfig;
    console.log(ftpConfig);
    console.log(
      user_id,
      site_url,
      `https://${site_url}`,
      `https://${site_proxy_domain}`,
    );
    return axios.post(HOSTING_SETUP_URL, {
      user: 'MakeStoriesUser',
      user_id,
      token: process.env.FIREBASE_ACCESS_TOKEN,
      name: site_url,
      site_url: `https://${site_url}`,
      site_proxy_domain: `https://${site_proxy_domain}`,
      ...ftpConfig,
    });
  }

  async requestFreeSSLCertificate(site: SiteInterface) {
    const {
      is_ssl_verified,
      domain_type,
      site_url,
      id: _id,
      site_proxy_domain,
      user_id,
    } = site || {};

    if (is_ssl_verified) {
      return true;
    }

    const url =
      domain_type === DOMAIN_TYPE.REVERSE_PROXY ? site_proxy_domain : site_url;

    const [sslCertificate, sslCertificateError] = await resolver(
      this.bunnycdnService.loadFreeCertificate(cleanURL(url)),
    );

    if (sslCertificateError) {
      return false;
    }

    const [, error] = await resolver(
      this.updateSiteDB(
        { id: _id },
        { is_ssl_verified: true, lp_status: true },
      ),
    );

    if (error) {
      return false;
    }

    const [, storyListingError] = await resolver(
      createStoryListingAPI({ userId: user_id, siteId: _id }),
    );

    console.log('story listing error', storyListingError);
    return true;
  }

  async requestVerification(site: SiteInterface) {
    const { is_verified, site_url, id: _id, user_id } = site || {};

    if (is_verified) {
      return true;
    }

    const [, verifyError] = await resolver(
      axios({
        method: 'get',
        url: `${site_url}/${_id}-verification.html`,
        timeout: 1000,
      }),
    );

    if (verifyError) {
      return false;
    }

    const [, error] = await resolver(
      this.updateSiteDB({ id: _id }, { is_verified: true, lp_status: true }),
    );

    if (error) {
      return false;
    }

    const [, storyListingError] = await resolver(
      createStoryListingAPI({ userId: user_id, siteId: _id }),
    );
    console.log('story listing error', storyListingError);

    return true;
  }

  requestVerificationForAll = async (site) => {
    await this.requestFreeSSLCertificate(site);
    await this.requestVerification(site);
  };

  async removeDomain(site: SiteInterface, awsService?: AwsService) {
    const { pull_zone_id, id: _id, site_proxy_domain, sp_id } = site || {};
    const type = identifyDomainType(site_proxy_domain);
    console.log('type:', type);
    console.log('site_proxy_domain', site_proxy_domain);
    if (type == 'CLOUDNS') {
      console.log('here inside loop');
      const [site, siteError] = await resolver(
        this.getSiteDB({
          id: _id,
        }),
      );
      guard({ error: siteError, message: 'Site not found' });
      guard({ condition: !site, message: 'Site not found' });
      console.log('SITE', site);
      if (site?.cf_distribution_id) {
        console.log('Deleting Distribution');
        const [, diserror] = await resolver(
          awsService.disableDistribution({
            distributionId: site.cf_distribution_id,
          }),
        );
        guard({ error: diserror, message: 'Unable to delete distribution' });
      }
      const [, error] = await resolver(
        Promise.all([
          // awsService.deleteCertificateDetails({
          //   certificateKey: site?.certificate_id,
          // }),
          deleteClouDnsRecord(site?.ssl_dns_id),
          deleteClouDnsRecord(site?.dns_id),
        ]),
      );
      console.log('Error delete site', error);
      guardV2({
        error,
        message: error?.code || 'Some Error occurred while deleting site',
      });
    } else {
      const zoneID = identifyZoneID(site_proxy_domain);
      //Remove Stackpath site if exist
      if (sp_id) {
        const [siteRemove, siteRemoveError] = await resolver(
          this.stackpathService.removeSite(sp_id),
        );

        guardV2({
          condition:
            siteRemoveError && siteRemoveError?.response?.status !== 404,
          message: 'Unable to delete stackpath site',
        });
      }

      // Remove Bunny pullzone
      if (pull_zone_id) {
        const [pullZoneDelete, pullZoneDeleteError] = await resolver(
          this.bunnycdnService.deletePullZone(pull_zone_id),
        );
        console.log({ pullZoneDeleteError });

        guardV2({
          error: pullZoneDeleteError,
          message: 'Unable to delete pullzone',
        });
      }

      const cleanSiteProxy = cleanURL(site_proxy_domain);
      const stackpathName = `cdn-stackpath-${cleanSiteProxy}`;
      const bunnyName = `cdn-bunny-${cleanSiteProxy}`;

      const [dnsDelete, dnsDeleteError] = await resolver(
        deleteDnsRecord(cleanSiteProxy, zoneID),
      );

      console.log({ dnsDeleteError });
      guardV2({
        error: dnsDeleteError,
        message: 'Unable to delete main DNS pointing',
      });

      const [stackpathBkpDNSDelete, stackpathBkpDNSDeleteError] =
        await resolver(deleteDnsRecord(stackpathName, zoneID));

      console.log({ stackpathBkpDNSDeleteError });
      guardV2({
        error: stackpathBkpDNSDeleteError,
        message: 'Unable to delete stackpath backup DNS record',
      });

      const [bunnyBkpDNSDelete, bunnyBkpDNSDeleteError] = await resolver(
        deleteDnsRecord(bunnyName, zoneID),
      );

      console.log({ bunnyBkpDNSDeleteError });
      guardV2({
        error: bunnyBkpDNSDeleteError,
        message: 'Unable to delete bunny backup DNS record',
      });
    }
    const [siteDelete, siteDeleteError] = await resolver(
      axios.get(`${makestoriesNodeApiBaseUrl}/hosting/site-delete`, {
        params: {
          token: process.env.SERVER_TOKEN,
          site: _id,
          user: 'MakeStoriesUser',
        },
      }),
    );

    console.log({ siteDeleteError });
    guardV2({
      error: siteDeleteError,
      message: 'Unable to delete site from DB',
    });
  }

  checkVerification = async (site: SiteInterface) => {
    const [data, error] = await resolver(
      this.getFTPDetail({ site_id: site?.id }),
    );
    const { port = null } = data ?? {};

    if (port) {
      if (port !== '22') {
        console.log(`${site.id}: PORT --> ${port}\n`);
        appendFileSync('log.txt', `${site.id}: PORT --> ${port}\n`);
        return true;
      }
    }

    if (site.is_verified && site.is_ssl_verified) {
      console.log(`${site.id}: SSL Certificate --> true`);
      console.log(`${site.id}: Site Verified --> true`);
      console.log(`${site.id}: Already verfied\n\n`);

      appendFileSync(
        'log.txt',
        `${site.id}: SSL Certificate --> true\n${site.id}: Site Verified --> true\n${site.id}: Already verfied\n\n`,
      );

      return true;
    }

    const currentDate: any = new Date();
    const createdDate: any = new Date(site.created_at);

    console.log({ createdDate });
    const difference = currentDate - createdDate;
    const days = difference / (1000 * 60 * 60 * 24);

    const [sslCertificate] = await resolver(
      this.requestFreeSSLCertificate(site),
    );

    console.log(`${site.id}: SSL Certificate --> ${sslCertificate}`);
    appendFileSync(
      'log.txt',
      `${site.id}: SSL Certificate --> ${sslCertificate}\n`,
    );

    if (!sslCertificate) {
      if (!site.is_verified) {
        const [sitemap] = await resolver(
          generateSiteMap({ siteID: site.id, userID: site.user_id }),
        );

        console.log(`${site.id}: Sitemap generation -->`, sitemap?.data);
        appendFileSync(
          'log.txt',
          `${site.id}: Sitemap generation --> ${JSON.stringify(
            sitemap?.data,
          )}\n`,
        );
      }

      const [siteVerfied] = await resolver(this.requestVerification(site));
      console.log(`${site.id}: Site Verified --> ${siteVerfied}`);
      appendFileSync(
        'log.txt',
        `${site.id}: Site Verified --> ${siteVerfied}\n`,
      );

      console.log({ days });
      if (days > 7 && !siteVerfied) {
        const [response, error] = await resolver(
          getStoriesCountAPI(site?.channel_id),
        );

        if (error) {
          console.log(`${site.id}: Error --> ${error.message}`);
          appendFileSync('log.txt', `${site.id}: Error --> ${error.message}\n`);
          return;
        }

        const storiesCount = response?.data?.total_stories;

        if (storiesCount === 0) {
          console.log(`${site.id}: Stories --> ${storiesCount}`);
          appendFileSync('delete.txt', `${JSON.stringify(site)}\n\n`);
          console.log(`${site.id}: Adding to Delete`);

          appendFileSync(
            'log.txt',
            `${site.id}: Stories --> ${storiesCount}\n${site.id}: Adding to Delete\n\n`,
          );
          // ! CRITICAL
          await resolver(this.removeDomain(site));
        }

        if (storiesCount > 0) {
          if (site?.warning_sent) {
            // If warning already sent
            const currentDate: any = new Date();
            const warningDate: any = new Date(site?.warning_sent);
            const difference = currentDate - warningDate;
            const days = difference / (1000 * 60 * 60 * 24);

            if (days > 2) {
              console.log(`${site.id}: Adding to Delete`);

              appendFileSync(
                'log.txt',
                `${site.id}: Stories --> ${storiesCount}\n${site.id}: Adding to Delete\n\n`,
              );

              // ! CRITICAL
              await resolver(this.removeDomain(site));
            }
          } else {
            console.log(`${site.id}: Stories --> ${storiesCount}`);
            appendFileSync('warning.txt', `${JSON.stringify(site)}\n\n`);
            console.log(`${site.id}: Adding to Warning`);

            appendFileSync(
              'log.txt',
              `${site.id}: Stories --> ${storiesCount}\n${site.id}: Adding to Warning\n\n`,
            );

            await sendWarningEmail({
              userId: site.user_id,
              domain: site.site_url,
            });

            this.updateSiteDB({ id: site.id }, { warning_sent: new Date() });
          }
        }
      }
    }

    console.log('\n');
    appendFileSync('log.txt', '\n');
  };

  async traveseSites(callback: any) {
    let page = 1;
    const limit = 1;

    while (true) {
      console.log(`-------- PAGE: ${page} --------`);
      appendFileSync('log.txt', `-------- PAGE: ${page} --------\n`);
      const [sitesData, sitesError] = await resolver(
        this.getSitesPaginated({ limit, page }),
      );

      const [sites, siteCount] = sitesData;

      const promises = sites.map((site) => callback(site));

      const [success, error] = await resolver(Promise.all(promises));

      if (!sites.length) break; // If no more sites
      if (page > Math.ceil(siteCount / limit)) break; // if no more pages

      page++;
    }
  }

  // *** CRON JOBS ***
  // @Cron(CronExpression.EVERY_2_HOURS)
  async requestFreeSSLCertificateCRON() {
    await resolver(this.traveseSites(this.requestVerificationForAll));
  }

  async deleteCertificateDNSAndSite(site) {
    const [, delErr] = await resolver(
      this.awsCouldNsService.deleteCertificateDetails({
        certificateKey: site?.certificate_id,
      }),
    );
    console.log('Error delete site', delErr);
    console.error(delErr);
    if (delErr) {
      guard({
        error: delErr,
        message:
          'Site Validation Failed, Please delete and re-create your domain',
      });
    }
    const [, errorDeletingDns] = await resolver(
      Promise.all([
        // awsService.deleteCertificateDetails({
        //   certificateKey: site?.certificate_id,
        // }),
        deleteClouDnsRecord(site?.ssl_dns_id),
        deleteClouDnsRecord(site?.dns_id),
        axios.get(`${makestoriesNodeApiBaseUrl}/hosting/site-delete`, {
          params: {
            token: process.env.SERVER_TOKEN,
            site: site.id,
            user: 'MakeStoriesUser',
          },
        }),
      ]),
    );
    console.log('Error delete site dns and db record', errorDeletingDns);
    if (errorDeletingDns) {
      guard({
        error: errorDeletingDns,
        message: 'Site Validation Failed, Please recreate your domain',
      });
    }

    const [planData, planError] = await resolver(
      planDetailsApi({ userId: site?.user_id }),
    );

    // guard({ error: planError });
    if (planError) {
      console.error(`Error occurred on getting plan details - ${planError}`);
    }

    const [activeCount, activeCountError] = await resolver(
      this.getActiveSitesCount({
        user_id: site?.user_id,
      }),
    );

    // guard({error: activeCountError})
    if (activeCountError) {
      console.error(
        `Error occurred while fetching sites count - ${activeCountError}`,
      );
    }

    const customDomains =
      planData?.data?.data?.planConfig?.customDomains ?? null;
    const domainLimit = customDomains?.limit ?? 0;

    if (domainLimit !== 'unlimited' && activeCount >= domainLimit) {
      const [checkModifier, checkError] = await resolver(
        checkModifierApi({
          userId: site?.user_id,
          type: 'domain',
        }),
      );

      if (checkError) {
        console.error(
          `Error occurred while checking modifiers - ${checkError}`,
        );
      }

      if (checkModifier?.data?.data && !checkError) {
        const [, handleModifierError] = await resolver(
          handleModifierApi({
            type: 'domain',
            action: 'remove',
            userId: site?.user_id,
          }),
        );

        // guard({ error: handleModifierError });
        if (handleModifierError) {
          console.error(
            `Error occurred on handling modifier - ${handleModifierError}`,
          );
        }
      }
    }

    guard({
      error: 'Domain Deleted',
      message:
        'Site Validation Failed, your Domain is deleted, Please recreate your domain',
    });
  }

  async verifyCloudNs(site: SiteInterface, awsService?: AwsService) {
    const domain = getDomain(`${site?.site_url}/`);
    const [isSiteVerified, checkErr] = await resolver(
      this.userSitesModel.findOne({
        where: {
          id: site.id,
          is_verified: true,
          is_ssl_verified: true,
        },
      }),
    );

    if (checkErr)
      console.error(
        `Error occurred while checking the site status - ${checkErr}`,
      );

    if (isSiteVerified?.is_verified && !checkErr) {
      return sendResponse({ message: 'Site already verified' });
    }
    const [CertificateDetails, error] = await resolver(
      awsService.getCertificateDetails({
        certificateKey: site?.certificate_id,
      }),
    );
    if (error) {
      const [awsData, error] = await resolver(
        awsService.requestCertificate({ domain: domain }),
      );
      guard({ error, message: error?.code || 'Unable to verify' });
      const [, updateErr] = await resolver(
        this.updateSiteDB(
          { id: site.id },
          {
            certificate_id: awsData?.CertificateArn,
          },
        ),
      );
      guard({
        error: updateErr,
        message: updateErr?.code || 'Unable to verify',
      });
      return await resolver(this.verifyCloudNs(site, awsService));
    }
    const Certificate = CertificateDetails?.Certificate;
    console.log(Certificate);
    if (Certificate.Status === 'ISSUED') {
      const [cloudFrontData, cloudFrontError] = await resolver(
        awsService.createDistribution({
          certificateARN: site?.certificate_id,
          domain: domain,
        }),
      );
      console.log(cloudFrontError);
      guard({
        error: cloudFrontError,
        message: 'Unable to create distribution',
      });
      const [cloudDnsData, cloudDnserror] = await resolver(
        addCloudNsRecord({
          'record-type': 'CNAME',
          'domain-name': INTERNAL_DOMAIN,
          host: makeInternalDomain(domain).name,
          record: cloudFrontData.Distribution.DomainName,
          ttl: 60,
          proxied: false,
        }),
      );
      console.log({ cloudDnsData, cloudDnserror });
      guard({ error: cloudDnserror, message: 'Unable to add dns record' });
      if (cloudDnsData?.data?.status === 'Failed') {
        guard({
          error: cloudDnsData?.data?.statusDescription,
          message: 'Unable to add dns record',
        });
      }
      //update user_site
      const [, updateErr] = await resolver(
        this.updateSiteDB(
          { id: site.id },
          {
            is_verified: true,
            is_ssl_verified: true,
            cf_distribution_id: cloudFrontData.Id,
            dns_id: cloudDnsData?.data?.data?.id,
          },
        ),
      );
      console.log(updateErr);
      return sendResponse({ message: 'Site is verified' });
    }
    if (Certificate.Status === 'FAILED') {
      return await resolver(this.deleteCertificateDNSAndSite(site));
    }
    if (Certificate.Status === 'VALIDATION_TIMED_OUT') {
      return await resolver(this.deleteCertificateDNSAndSite(site));
    }
    guard({
      error: 'PENDING_VALIDATION',
      message: 'Dns validation in progress, Please check back in 30 minutes',
    });
  }

  async addCloudNsSite(
    workspace: string,
    domain: string,
    subDomain: string,
    type,
    awsService: AwsService,
  ) {
    const isDomainValid = validate({
      domain,
      keywords: BLOCKED_DOMAINS,
      exact: true,
    });

    const isSubdomainValid = validate({
      domain: subDomain,
      keywords: BLOCKED_KEYWORDS,
      exact: true,
    });

    if (type === DOMAIN_TYPE.SUBSOMAIN) {
      guard({
        condition: !isSubdomainValid,
        message: 'Subdomain is not allowed',
      });
    }

    guard({
      condition: !isDomainValid,
      message: 'Domain is not allowed',
    });

    const cleanDomain = cleanURL(domain).toLowerCase();
    const cleanSubdomain = cleanURL(subDomain).toLowerCase();

    guard({
      condition: cleanDomain === cleanSubdomain,
      message: 'Domain and Sub Domain should not be same',
    });

    guard({
      condition: cleanSubdomain.includes('.com'),
      message: 'Subdomain should not contain ".com"',
    });

    const domains = {
      subDomain: `${replaceSlash(cleanSubdomain)}.${cleanDomain}`,
      reverseProxyDomain: `${cleanDomain}/${cleanSubdomain}`,
      isReverseProxy: type === DOMAIN_TYPE.REVERSE_PROXY,

      checkIsOurDomain() {
        return domain === OUR_DOMAIN && this.isReverseProxy;
      },

      getMainDomain() {
        return this.isReverseProxy ? this.reverseProxyDomain : this.subDomain;
      },
      getInternalDomain() {
        return makeInternalDomain(this.subDomain);
      },
    };

    const isOurDomain = domains.checkIsOurDomain();
    const internal = domains.getInternalDomain();

    console.log({
      domains,
      isOurDomain,
      internal,
      mainDomain: domains.getMainDomain(),
    });

    // Checking if domain Exist
    const withHttps = `https://${internal.domain}`;
    const [existingDomain, existingDomainError] = await resolver(
      this.getSiteDB({
        site_proxy_domain: withHttps,
      }),
    );

    const condition =
      existingDomain?.site_proxy_domain === withHttps &&
      existingDomain?.deleted_at === null;

    console.log({ existingDomain, existingDomainError, condition, withHttps });
    guard({
      condition,
      message: 'Domain already exist',
      code: 409,
    });

    console.log('after guard 1');
    let certificate_id = null;
    let ssl_dns_id = null;
    let cname, cvalue;
    let cNameError = null;
    let cf_distribution_id = null;
    if (!isOurDomain) {
      let certificateArn = null;
      if (domains.isReverseProxy) {
        certificateArn = ARN;
        const [cloudFrontData, cloudFrontError] = await resolver(
          awsService.createDistribution({
            certificateARN: certificateArn,
            domain: internal?.domain,
          }),
        );
        console.log(cloudFrontError);
        cNameError = cloudFrontError;
        if (!cloudFrontError) {
          cname = internal?.domain;
          cvalue = cloudFrontData.Distribution.DomainName;
          cf_distribution_id = cloudFrontData.Id;
        }
      } else {
        const [awsData, error] = await resolver(
          awsService.requestCertificate({ domain: domains.getMainDomain() }),
        );

        console.log('awsData: ', awsData);
        guard({ error, message: error?.code || 'AWS Error occured' });
        certificateArn = awsData?.CertificateArn;
        let i = 0;
        while (i <= 5 && !cname) {
          await sleep(2000);
          const [certificateData, err] = await resolver(
            awsService.getCertificateDetails({
              certificateKey: awsData.CertificateArn,
            }),
          );
          cNameError = err;
          cname =
            certificateData?.Certificate?.DomainValidationOptions?.[0]
              ?.ResourceRecord?.Name;
          cvalue =
            certificateData?.Certificate?.DomainValidationOptions?.[0]
              ?.ResourceRecord?.Value;
          i++;
        }
      }
      if (!cname && cNameError) {
        guard({
          error: cNameError,
          message: cNameError?.code || 'SSL certificate error occured',
        });
      }
      const [mainDnsRecord, mainDnsRecordError] = await resolver(
        addCloudNsRecord({
          'record-type': 'CNAME',
          'domain-name': INTERNAL_DOMAIN,
          host: domains.isReverseProxy
            ? `${internal?.name}`
            : `ssl-${internal?.name}`,
          record: cvalue,
          ttl: 60,
        }),
      );
      console.log({ mainDnsRecord, mainDnsRecordError });
      guard({ error: mainDnsRecordError, message: 'Unable to add dns record' });
      if (mainDnsRecord?.data?.status === 'Failed') {
        guard({
          error: mainDnsRecord?.data?.statusDescription,
          message: 'Unable to add dns record',
        });
      }
      certificate_id = certificateArn || null;
      ssl_dns_id = mainDnsRecord?.data?.data?.id || null;
    }

    const [channelCreated, channelCreationError] = await resolver(
      this.createChannel(
        {
          user_id: workspace,
          site_url: domains.getMainDomain(),
          site_proxy_domain: internal?.domain,
        },
        isOurDomain,
        domains.isReverseProxy,
      ),
    );
    guard({ error: channelCreationError, message: 'Unable to create Channel' });
    let postData: any = {
      domain_type: type,
      ssl_cname_record: !isOurDomain ? cname : null,
    };
    if (!domains.isReverseProxy) {
      postData = {
        ...postData,
        is_verified: isOurDomain,
        certificate_id: certificate_id,
        ssl_dns_id: ssl_dns_id,
        ssl_cname_value: !isOurDomain ? `ssl-${internal?.domain}` : null,
      };
    } else {
      postData = {
        ...postData,
        certificate_id: null,
        is_verified: true,
        dns_id: ssl_dns_id,
        ssl_cname_value: cvalue,
        cf_distribution_id: cf_distribution_id,
      };
    }
    console.log('Save record in db', postData);
    const [saveData, saveError] = await resolver(
      this.updateSiteDB({ id: channelCreated?.data?.siteId }, { ...postData }),
    );
    console.log(saveError);
    console.log('Saved record in DB');
    guard({ error: saveError, message: 'Unable to save site in database' });
    return sendResponse({
      data: {
        domain: domains.getMainDomain(),
        domainType: type,
        deliveryDomain: internal?.domain,
        records:
          isOurDomain || domains.isReverseProxy
            ? []
            : [
                {
                  domain: domains.getMainDomain(),
                  deliveryDomain: internal?.domain,
                },
                {
                  domain: cname,
                  deliveryDomain: `ssl-${internal?.domain}`,
                },
              ],
      },
    });
  }

  async invalidateDistribution(siteId) {
    const [site, error] = await resolver(
      this.getSiteDB({
        id: siteId,
      }),
    );
    guard({
      error: error,
      message: 'SITE NOT FOUND',
    });
    if (site?.cf_distribution_id) {
      const [, invalidatedError] = await resolver(
        this.awsCouldNsService.invalidateDistribution(site.cf_distribution_id),
      );
      guard({
        error: invalidatedError,
        message: 'INVALIDATION FAILURE',
      });
      return sendResponse({ message: 'Site Invalidated successfully' });
    }
    guard({
      error: 'DISTRIBUTION NOT FOUND',
      message: 'DISTRIBUTION NOT FOUND',
    });
  }

  // @Cron(CronExpression.EVERY_DAY_AT_MIDNIGHT)
  removeDomainCRON() {
    return this.traveseSites(this.checkVerification);
  }

  // @Cron(CronExpression.EVERY_10_MINUTES)
  async removeDomainDistribution() {
    const [sites, error] = await resolver(
      this.getDeletedSites({
        cf_distribution_id: Not(IsNull()),
      }),
    );
    guard({
      error: error,
      message: 'Error fetching deleted sites',
    });

    await Promise.all(
      sites.map(async (site) => {
        const [distribution, disError] = await resolver(
          this.awsCouldNsService.getDistribution({
            distributionId: site.cf_distribution_id,
          }),
        );
        if (
          !disError &&
          distribution?.Distribution?.DistributionConfig?.Enabled === false
        ) {
          const [, delDisError] = await resolver(
            this.awsCouldNsService.deleteDistribution({
              distributionId: distribution?.Distribution?.Id,
              ETag: distribution?.ETag,
            }),
          );
          if (!delDisError) {
            if (site?.certificate_id !== ARN) {
              const [, certificateDeleteError] = await resolver(
                this.awsCouldNsService.deleteCertificateDetails({
                  certificateKey: site?.certificate_id,
                }),
              );
              console.log(certificateDeleteError);
            }
            await this.updateDeletedSiteDB(
              { id: site.id },
              { cf_distribution_id: null },
            );
          }
        }
        console.log('here');
        return true;
      }),
    );
    return sendResponse({ message: 'Done deleting distribution' });
  }

  async updateDistribution() {
    const [sites, error] = await resolver(
      this.userSitesModel.find({
        where: {
          // cf_distribution_id: Not(IsNull()),
          created_at: Between(
            new Date('2022-08-22 00:00:00'),
            new Date('2022-08-23 23:59:59'),
          ),
          certificate_id: IsNull(),
          domain_type: DOMAIN_TYPE.SUBSOMAIN,
          ssl_dns_id: Not(IsNull()),
          dns_id: IsNull(),
        },
        take: 1,
      }),
    );
    guard({
      error: error,
      message: 'No Sites Found',
    });
    if (sites.length === 0) {
      guard({
        error: error,
        message: 'No Sites Found',
      });
    }
    // console.log(sites);
    // return sendResponse({ message: 'Done updating distribution' });
    await Promise.all(
      sites.map(async (site) => {
        const domain = getDomain(`${site?.site_url}/`);
        let certificate_id = site?.certificate_id;
        let verifyError = null;
        const [, updateSiteError] = await resolver(
          this.updateSiteDB(
            { id: site.id },
            {
              is_verified: false,
              ssl_dns_id: site?.dns_id,
              dns_id: null,
              ssl_cname_value: `ssl-${makeInternalDomain(domain).domain}`,
            },
          ),
        );
        if (!certificate_id) {
          const getCertificateArn = async (NextToken?: string) => {
            let params: any = {
              CertificateStatuses: ['ISSUED'],
            };
            if (NextToken) {
              params = {
                ...params,
                NextToken,
              };
            }
            const [list, listError] = await resolver(
              this.awsCouldNsService.listCertificates({
                ...params,
              }),
            );
            if (list) {
              const { CertificateSummaryList = [], NextToken } = list;
              const certificateArnIndex = CertificateSummaryList.findIndex(
                (l) => l.DomainName === domain,
              );
              if (certificateArnIndex === -1) {
                return getCertificateArn(NextToken);
              }
              return CertificateSummaryList[certificateArnIndex].CertificateArn;
            }
          };
          certificate_id = await getCertificateArn();
          site.certificate_id = certificate_id;
          const [, updateSiteError] = await resolver(
            this.updateSiteDB(
              { id: site.id },
              {
                certificate_id: certificate_id,
              },
            ),
          );
          const [distribution, disError] = await resolver(
            this.verifyCloudNs(site, this.awsCouldNsService),
          );
          verifyError = disError;
        } else {
          const [distribution, disError] = await resolver(
            this.verifyCloudNs(site, this.awsCouldNsService),
          );
          verifyError = disError;
        }
        console.log('verifyDnsError:', verifyError);
        // guard({
        //   error: disError,
        //   message: disError?.message || 'Failed to verify Site',
        // });
        // const [distribution, disError] = await resolver(
        //   this.awsCouldNsService.getDistribution({
        //     distributionId: site.cf_distribution_id,
        //   }),
        // );
        // console.log(distribution);
        // if (!disError) {
        //   const domain = getDomain(`${site?.site_url}/`);
        //   console.log(domain);
        //   const [UpdatedDistribution, disError] = await resolver(
        //     this.awsCouldNsService.updateDistribution({
        //       DistributionConfig: {
        //         ...distribution?.Distribution?.DistributionConfig,
        //         DefaultCacheBehavior: {
        //           ...distribution?.Distribution?.DistributionConfig
        //             ?.DefaultCacheBehavior,
        //           ViewerProtocolPolicy: 'redirect-to-https',
        //         },
        //         // Origins: {
        //         //   ...distribution?.Distribution?.DistributionConfig?.Origins,
        //         //   Items: [
        //         //     {
        //         //       ...distribution?.Distribution?.DistributionConfig?.Origins
        //         //         ?.Items[0],
        //         //       CustomHeaders: {
        //         //         Items: [
        //         //           {
        //         //             HeaderName: 'domain',
        //         //             HeaderValue: domain,
        //         //           },
        //         //         ],
        //         //         Quantity: 1,
        //         //       },
        //         //     },
        //         //   ],
        //         // },
        //         // Aliases: {
        //         //   Quantity: 1,
        //         //   Items: [domain],
        //         // },
        //       },
        //       distributionId: site?.cf_distribution_id,
        //       ETag: distribution?.ETag,
        //     }),
        //   );
        //   console.log(disError);
        // if (!verifyError) {
        console.log('here');
        return true;
      }),
    );
    return sendResponse({ message: 'Done updating distribution' });
  }

  // @Cron(CronExpression.EVERY_30_MINUTES)
  async requestVerificationCron() {
    let page = 1;
    const limit = 10;
    while (true) {
      const [sitesData, sitesError] = await resolver(
        this.getSitesPaginated(
          {
            page,
            limit,
          },
          {
            certificate_id: Not(IsNull()),
            is_verified: false,
            ssl_dns_id: Not(IsNull()),
            domain_type: DOMAIN_TYPE.SUBSOMAIN,
          },
        ),
      );
      const [sites, siteCount] = sitesData;
      await Promise.all(
        sites.map(async (site) => {
          const [, disError] = await resolver(
            this.verifyCloudNs(site, this.awsCouldNsService),
          );
          console.log(disError);
          return true;
        }),
      );
      if (!sites.length) break; // If no more sites
      if (page > Math.ceil(siteCount / limit)) break; // if no more pages
      await sleep(2000);
      page++;
    }
    return sendResponse({ message: 'Done Requesting Certificates' });
  }

  // return all workspace ids including userid
  async getAllWorkspaceId(workspace) {
    const userRecord = await this.usersModel.findOne({
      where: { id: workspace },
    });

    let teamOwner: string;
    if (userRecord) {
      teamOwner = workspace;
    }

    if (!teamOwner) {
      const teamrecord = await this.teamsModel.findOne({
        where: { id: workspace },
      });
      if (teamrecord) teamOwner = teamrecord.owner;
    }

    const teams = await this.teamsModel.find({
      where: { owner: teamOwner },
      select: ['id'],
    });

    const teamIds = [teamOwner];
    if (teams.length) {
      teams.forEach((eachTeam) => {
        teamIds.push(eachTeam.id);
      });
    }

    return teamIds;
  }
}
