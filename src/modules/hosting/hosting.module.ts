import { HostingService } from './hosting.service';
import { HostingController } from './hosting.controller';
import { Module } from '@nestjs/common';
import { StackpathModule } from '../stackpath/stackpath.module';
import { MetricsModule } from '../metrics/metrics.module';
import { BunnycdnModule } from '../bunnycdn/bunnycdn.module';
// import { ScheduleModule } from '@nestjs/schedule';
import { TypeOrmModule } from '@nestjs/typeorm';

import { UserSites } from './site.entity';
import { FTPDetails } from './ftpdetails.entity';
import { AwsService } from '../aws/aws.service';
import { Users } from 'makestories-library';
import { Teams } from 'makestories-library';

@Module({
  imports: [
    // ScheduleModule.forRoot(),
    TypeOrmModule.forFeature([UserSites, FTPDetails, Users, Teams]),
    StackpathModule,
    MetricsModule,
    BunnycdnModule,
  ],
  controllers: [HostingController],
  providers: [HostingService, AwsService],
})
export class HostingModule {}
