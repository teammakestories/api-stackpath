import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  DeleteDateColumn,
} from 'typeorm';

@Entity({ name: 'ftp_details' })
export class FTPDetails {
  @PrimaryGeneratedColumn({ type: 'int' })
  id: number;

  @Column({ type: 'int', default: null, nullable: true })
  site_id: number;

  @Column({ type: 'varchar', default: null, nullable: false, length: 255 })
  host: string;

  @Column({ type: 'varchar', default: null, nullable: false, length: 255 })
  password: string;

  @Column({ type: 'varchar', default: null, nullable: false, length: 255 })
  username: string;

  @Column({ type: 'varchar', default: null, nullable: false, length: 255 })
  base_path: string;

  @Column({ type: 'varchar', default: null, nullable: false, length: 255 })
  port: string;

  @Column({ type: 'tinyint', default: 0, nullable: true })
  append_sid: boolean;

  @Column({
    type: 'timestamp',
    nullable: true,
    default: () => 'CURRENT_TIMESTAMP',
  })
  created_at: Date;

  @Column({
    type: 'timestamp',
    nullable: true,
    default: () => 'CURRENT_TIMESTAMP',
  })
  updated_at: Date;

  @Column({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP',
    nullable: true,
  })
  @DeleteDateColumn()
  deleted_at: Date;
}
