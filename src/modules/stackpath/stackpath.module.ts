import { StackpathService } from './stackpath.service';
import { Module } from '@nestjs/common';

@Module({
  imports: [],
  controllers: [],
  providers: [StackpathService],
  exports: [StackpathService],
})
export class StackpathModule {}
