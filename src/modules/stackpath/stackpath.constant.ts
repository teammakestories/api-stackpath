export const STACK_ID = '32d6adeb-b5f3-4322-bbdc-1b664ee78b13';

export const STACKPATH_API_URI = Object.freeze({
  baseURl: 'https://gateway.stackpath.com',
  token: '/identity/v1/oauth2/token',
  pops: '/cdn/v1/pops',
  delivery: `/delivery/v1/stacks/${STACK_ID}`,
  sites: `/delivery/v1/stacks/${STACK_ID}/sites`,
  metrics: `/delivery/v1/stacks/${STACK_ID}/metrics`,
  freeCertificateUri: (siteId: string) =>
    `/cdn/v1/stacks/${STACK_ID}/sites/${siteId}/certificates/request`,

  deliveryDomainUri: (siteId: string) =>
    `/delivery/v1/stacks/${STACK_ID}/sites/${siteId}/delivery_domains`,

  cdnUri: (siteId: string) =>
    `/delivery/v1/stacks/${STACK_ID}/sites/${siteId}/cdn`,
});
