import { Injectable } from '@nestjs/common';
import axios from 'axios';
import { getCloudflareURL } from 'src/api/cloudflare/cloudflare.constant';
import { STACKPATH_API_URI } from './stackpath.constant';

import {
  CDNControlInterface,
  GetMetricsInterface,
  Token,
} from './stackpath.interface';

@Injectable()
export class StackpathService {
  private token: Token;
  private readonly api: any;
  constructor() {
    this.api = axios.create({
      baseURL: STACKPATH_API_URI.baseURl,
    });

    this.addSite = this.addSite.bind(this);
    this.addDeliveryDomain = this.addDeliveryDomain.bind(this);
    this.getDeliveryDomains = this.getDeliveryDomains.bind(this);
    this.getSites = this.getSites.bind(this);
    this.getMetrics = this.getMetrics.bind(this);
    this.checkTokenValidity = this.checkTokenValidity.bind(this);
    this.cloudflareDnsEntry = this.cloudflareDnsEntry.bind(this);
  }

  async addSite(domain: string, isOurDomain: boolean) {
    const headers = this.getHeader();

    const hostname = isOurDomain
      ? process.env.FTP_HOSTNAME_2
      : process.env.FTP_HOSTNAME;

    const data = {
      domain,
      origin: {
        http: {
          hostname,
        },
      },
    };
    return await this.api.post(STACKPATH_API_URI.sites, data, { headers });
  }

  async removeSite(siteId: string) {
    await this.generateToken();
    const headers = this.getHeader();
    return this.api.delete(`${STACKPATH_API_URI.sites}/${siteId}`, {
      headers,
    });
  }

  requestFreeSSL(siteId: string) {
    const headers = this.getHeader();
    return this.api.post(
      STACKPATH_API_URI.freeCertificateUri(siteId),
      {},
      { headers },
    );
  }

  addDeliveryDomain(domain: string, siteId: string) {
    const headers = this.getHeader();
    return this.api.post(
      `${STACKPATH_API_URI.sites}/${siteId}/delivery_domains`,
      { domain },
      { headers },
    );
  }

  getDeliveryDomains(siteId: string) {
    const headers = this.getHeader();
    return this.api.get(
      `${STACKPATH_API_URI.sites}/${siteId}/delivery_domains`,
      { headers },
    );
  }

  getSites() {
    const headers = this.getHeader();
    return this.api.get(STACKPATH_API_URI.sites, { headers });
  }

  cdnControl({ siteId = '', enabled = false }: CDNControlInterface) {
    const headers = this.getHeader();
    if (enabled) {
      return this.api.post(STACKPATH_API_URI.cdnUri(siteId), {}, { headers });
    }

    return this.api.delete(STACKPATH_API_URI.cdnUri(siteId), { headers });
  }

  getPops() {
    const headers = this.getHeader();
    return this.api.get(STACKPATH_API_URI.pops, { headers });
  }

  getMetrics(params: GetMetricsInterface) {
    const headers = this.getHeader();
    return this.api.get(STACKPATH_API_URI.metrics, { params, headers });
  }

  async checkTokenValidity() {
    if (this.token) {
      if (this.isExpired()) {
        await this.generateToken();
      }
    } else {
      await this.generateToken();
    }
  }

  cloudflareDnsEntry({ type, name, content }) {
    return axios.post(
      getCloudflareURL(),
      {
        type,
        name,
        content,
      },
      {
        headers: {
          Authorization: `Bearer ${process.env.CLOUDFLARE_TOKEN}`,
          'Content-Type': 'application/json',
        },
      },
    );
  }

  // Private Methods
  private isExpired() {
    const currentTime = new Date().getTime() / 1000;
    const timeDifference = currentTime - this.token.createdAt;
    return timeDifference >= this.token.expires;
  }

  private async generateToken() {
    const {
      data: { access_token, token_type, expires_in },
    }: any = await this.api.post(STACKPATH_API_URI.token, {
      grant_type: 'client_credentials',
      client_id: process.env.CLIENT_ID,
      client_secret: process.env.CLIENT_SECRET,
    });

    const token = {
      accessToken: access_token,
      type: token_type,
      expires: expires_in,
      createdAt: new Date().getTime() / 1000,
    };

    this.token = token;
  }

  private getHeader() {
    return {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${this.token.accessToken}`,
    };
  }

  getUsageFromMetrics(metric) {
    const metricData = metric?.data?.data?.matrix?.results ?? [];

    const totalUsageMetric = metricData?.find(
      (data) => data.metric.__name__ === 'transfer_used_total_mb',
    );

    const reducer = (accumulator: any, current: any) =>
      accumulator + Number(current.value);

    return totalUsageMetric?.values?.reduce(reducer, 0) ?? 0;
  }
}
