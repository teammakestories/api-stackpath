export interface Token {
  accessToken: string;
  type: string;
  expires: number;
  createdAt: number;
}

export interface GetMetricsInterface {
  platforms: string;
  granularity: string;
  sites: string;
  start_date: string;
  end_date: string;
  group_by?: string;
}

export interface CDNControlInterface {
  siteId: string;
  enabled?: boolean;
}
