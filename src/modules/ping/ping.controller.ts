import { Controller, Get } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('Ping')
@Controller('ping')
export class PingController {
  @Get()
  ping() {
    return 'pong 5 april 2022';
  }

  @Get('readiness')
  async pingReadiness() {
    return 'Container started';
  }
}
