import { Injectable } from '@nestjs/common';
import { S3, ACM, CloudFront } from 'aws-sdk';
import * as tldjs from 'tldjs';
import { v4 as uuidv4 } from 'uuid';
import { guardV2, resolver } from 'nest-library';
import { OUR_DOMAIN } from '../hosting/hosting.constant';
import { CURRENT_DOMAIN_CONFIG } from 'src/config/app.config';
import { makeInternalDomain } from '../../utils/common.util';

const { INTERNAL_DOMAIN } = CURRENT_DOMAIN_CONFIG;

@Injectable()
export class AwsService {
  s3: S3;
  acm: ACM;
  cloudFront: CloudFront;
  constructor() {
    this.acm = new ACM({
      region: 'us-east-1',
      accessKeyId: process.env.AWS_ACCESS_KEY_ID,
      secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    });
    this.cloudFront = new CloudFront({
      region: 'us-east-1',
      accessKeyId: process.env.AWS_ACCESS_KEY_ID,
      secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    });
  }
  requestCertificate({ domain }) {
    const params = {
      DomainName: domain /* required */,
      DomainValidationOptions: [
        {
          DomainName: domain /* required */,
          ValidationDomain: tldjs.getDomain(domain) /* required */,
        },
      ],
      // SubjectAlternativeNames: [`*.${INTERNAL_DOMAIN}`],
      ValidationMethod: 'DNS',
    };
    return new Promise((resolve, reject) => {
      this.acm.requestCertificate(params, (err, data) => {
        if (err) {
          reject(err);
        }
        if (data) {
          resolve(data);
        }
      });
    });
  }
  getCertificateDetails(param) {
    const params = {
      CertificateArn: param.certificateKey,
    };
    return new Promise((resolve, reject) => {
      this.acm.describeCertificate(params, (err, data) => {
        if (err) {
          console.log(err);
          reject(err);
        }
        if (data) {
          console.log(data);
          resolve(data);
        }
      });
    });
  }
  deleteCertificateDetails(param) {
    const params = {
      CertificateArn: param.certificateKey,
    };
    return new Promise((resolve, reject) => {
      this.acm.deleteCertificate(params, (err, data) => {
        if (err) {
          reject(err);
        }
        if (data) {
          resolve(data);
        }
      });
    });
  }
  async invalidateDistribution(distributionId) {
    const [data, error] = await resolver(
      this.cloudFront
        .createInvalidation({
          DistributionId: distributionId,
          InvalidationBatch: {
            CallerReference: uuidv4(),
            Paths: {
              Quantity: 1,
              Items: ['/*'], //temporary change
            },
          },
        })
        .promise(),
    );
    guardV2({ error, message: 'Invalidating Failed' });
    return data;
  }
  createDistribution({ domain, certificateARN }) {
    const params: any = {
      DistributionConfig: {
        CallerReference: uuidv4(),
        Comment: 'Create Distribution',
        DefaultCacheBehavior: {
          TargetOriginId: domain,
          ViewerProtocolPolicy: 'redirect-to-https',
          AllowedMethods: {
            Items: ['GET', 'HEAD'],
            Quantity: '2',
          },
          CachePolicyId: '658327ea-f89d-4fab-a63d-7e88639e58f6',
          Compress: true,
        },
        Enabled: true,
        Origins: {
          Items: [
            {
              DomainName:
                domain === OUR_DOMAIN
                  ? process.env.STORIES_SITE_ORIGIN
                  : process.env.SHARE_ORIGIN,
              Id: domain,
              OriginShield: {
                Enabled: false,
              },
              CustomHeaders: {
                Items: [
                  {
                    HeaderName: 'domain',
                    HeaderValue: domain,
                  },
                ],
                Quantity: 1,
              },
              CustomOriginConfig: {
                HTTPPort: 80,
                HTTPSPort: 443,
                OriginProtocolPolicy: 'match-viewer',
              },
            },
          ],
          Quantity: 1,
        },
        Aliases: {
          Quantity: 1,
          Items: [domain],
        },
        HttpVersion: 'http2',
        IsIPV6Enabled: true,
        Logging: {
          Bucket: '',
          Enabled: false,
          IncludeCookies: false,
          Prefix: '',
        },
        PriceClass: 'PriceClass_All',
        ViewerCertificate: {
          ACMCertificateArn: certificateARN,
          CloudFrontDefaultCertificate: false,
          MinimumProtocolVersion: 'TLSv1.2_2021',
          SSLSupportMethod: 'sni-only',
        },
      },
    };
    return new Promise((resolve, reject) => {
      this.cloudFront.createDistribution(params, (err, data) => {
        if (err) {
          reject(err);
        }
        if (data) {
          resolve(data);
        }
      });
    });
  }

  updateDistribution({ DistributionConfig, distributionId, ETag }) {
    const params: any = {
      DistributionConfig: {
        ...DistributionConfig,
      },
      Id: distributionId,
      IfMatch: ETag,
    };
    return new Promise((resolve, reject) => {
      this.cloudFront.updateDistribution(params, (err, data) => {
        if (err) {
          reject(err);
        }
        if (data) {
          resolve(data);
        }
      });
    });
  }
  async disableDistribution({ distributionId }) {
    const data: any = await new Promise((resolve, reject) => {
      this.cloudFront.getDistributionConfig(
        { Id: distributionId },
        (error, obj) => {
          if (error) {
            reject(error);
          }
          if (obj) {
            resolve(obj);
          }
        },
      );
    });
    const { ETag, ...updateParams } = data;
    updateParams.DistributionConfig.Enabled = false;
    return new Promise((resolve, reject) => {
      this.cloudFront.updateDistribution(
        { Id: distributionId, IfMatch: ETag, ...updateParams },
        (error, obj) => {
          if (error) {
            reject(error);
          }
          if (obj) {
            resolve(obj);
          }
        },
      );
    });
  }
  async enableDistribution({ distributionId }) {
    const data: any = await new Promise((resolve, reject) => {
      this.cloudFront.getDistributionConfig(
        { Id: distributionId },
        (error, obj) => {
          if (error) {
            reject(error);
          }
          if (obj) {
            resolve(obj);
          }
        },
      );
    });
    const { ETag, ...updateParams } = data;
    updateParams.DistributionConfig.Enabled = true;
    return new Promise((resolve, reject) => {
      this.cloudFront.updateDistribution(
        { Id: distributionId, IfMatch: ETag, ...updateParams },
        (error, obj) => {
          if (error) {
            reject(error);
          }
          if (obj) {
            resolve(obj);
          }
        },
      );
    });
  }
  getDistribution({ distributionId }) {
    const params: any = {
      Id: distributionId,
    };
    return new Promise((resolve, reject) => {
      this.cloudFront.getDistribution(params, (err, data) => {
        if (err) {
          reject(err);
        }
        if (data) {
          resolve(data);
        }
      });
    });
  }
  deleteDistribution({ distributionId, ETag }) {
    const params: any = {
      Id: distributionId,
      IfMatch: ETag,
    };
    return new Promise((resolve, reject) => {
      this.cloudFront.deleteDistribution(params, (err, data) => {
        if (err) {
          reject(err);
        }
        if (data) {
          resolve(data);
        }
      });
    });
  }
  listCertificates(param) {
    return new Promise((resolve, reject) => {
      this.acm.listCertificates(param, (err, data) => {
        if (err) {
          reject(err);
        }
        if (data) {
          resolve(data);
        }
      });
    });
  }
}
