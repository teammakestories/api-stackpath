import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AuthMiddleware, AuthModule } from 'makestories-library';
import { PUBLIC_ROUTES } from './constants/route.constant';
import { BunnycdnModule } from './modules/bunnycdn/bunnycdn.module';
import { HostingController } from './modules/hosting/hosting.controller';
import { HostingModule } from './modules/hosting/hosting.module';
import { MetricsModule } from './modules/metrics/metrics.module';
import { PingModule } from './modules/ping/ping.module';
import { StackpathModule } from './modules/stackpath/stackpath.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MongooseModule } from '@nestjs/mongoose';
import { MONGODB_URI, getMongoConfig } from './config/app.config';

@Module({
  imports: [
    BunnycdnModule,
    PingModule,
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: '.env',
    }),
    MongooseModule.forRoot(MONGODB_URI, getMongoConfig()),
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: process.env.DB_MYSQL_HOST,
      port: Number(process.env.DB_MYSQL_PORT),
      username: process.env.DB_MYSQL_USERNAME,
      password: process.env.DB_MYSQL_PASSWORD,
      database: process.env.DB_MYSQL_DATABASE,
      synchronize: false,
      autoLoadEntities: true,
    }),
    AuthModule,
    StackpathModule,
    MetricsModule,
    HostingModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(AuthMiddleware)
      .exclude(...PUBLIC_ROUTES)
      .forRoutes(HostingController);
  }
}
